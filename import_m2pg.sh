#!/bin/bash
## Dig the WAL Virtualbox VM install script
## May work on Mac and Linux
#-----------------------------------------------------
# (c)2018 Loxodata PostgreSQL-like License

# Uncomment for debug
#set -x
OVAFILE="m2pg_2019-10-15.ova"
vm_prefix="m2pg"
ostype="RedHat_64"
finaluser="attendee"
finaleip="10.0.0.125"

# We use a "unique" name for the training
# we hope so.
vmname="${vm_prefix}_$(date +%Y-%m-%d)"
vboxes=$(vboxmanage list vms | grep -c ${vmname})
echo "========================================"
echo "Loxodata VM import Script"
echo "========================================"
# If there's no box import the one on the key
if [ ${vboxes} -eq 0 ]; then
    echo "There's no Loxodata VM in virtualbox, importing it"
    vboxmanage import  ${OVAFILE} --vsys 0 --ostype "${ostype}" --vmname $vmname  --eula accept
else
    echo "VM already exists".
fi

# If there's more than one box of our kind, exit
if [ ${vboxes} -gt 1 ]; then
    echo "There's ${vboxes} VM with \"${vmname}\" in their name"
    exit -1
fi

# Import went wrong, warn the user and exit
[[ $(vboxmanage list vms | grep -c ${vmname}) -lt 1 ]] && echo "Cannot find vm" && exit -2

# Ok vm is in VBox
echo "Using vm : ${vmname}"

# Check if running
if [ $(vboxmanage showvminfo ${vmname}|grep State|grep -c running) -eq 1 ]; then
    echo "VM ${vmname} is running : unable to configure. Stop it and run this script."
    echo "Try the following command"
    echo "vboxmanage controlvm ${vmname} poweroff"
    exit -3
fi

# Check if a vbox host only net is already defined
OS="$(uname)"
case $OS in
    'Linux')
	hashostonlyif=$(ip -4 -o -f inet a|grep vbox|grep 10.0.0)
	;;
    'Darwin')
	# To comply with Linux ip command format and keeping only first vboxadapter with 10.0.0
	hashostonlyif=$((echo -n "5: "&&`which ifconfig` |grep -e vbox -e inet|grep -A1 vbox|grep -B 1 10.0.0|head -1)|sed -e 's/://g')
	;;
    *)
	echo "unsupported ip config tool!"
	exit -4

 esac


# If so do not create the hostonly nic
if [ $(echo ${hashostonlyif}|grep -c 10.0.0) -eq 0  ]; then
    echo "Creating hostonly network"
    hostonlyif=$(vboxmanage hostonlyif create 2>/dev/null | grep Interface | sed "s/.*'\(.*\)'.*/\\1/")
    vboxmanage hostonlyif ipconfig ${hostonlyif} --ip 10.0.0.1 --netmask 255.255.255.0
else
    hostonlyif=$(echo ${hashostonlyif}|tail -1|awk '{print $2}')
    echo "Hostonly network already exists"
fi
echo "Using hostonly IF: ${hostonlyif}"

# Bind host iface with vbox adapter 2
echo "Using hostonly network ${hostonlyif} for ${vmname}"
vboxmanage modifyvm ${vmname} --nic2 hostonly
vboxmanage modifyvm ${vmname} --hostonlyadapter2 ${hostonlyif}

# Start VM and tell the user
echo "Starting vm: ${vmname}"
vboxmanage startvm ${vmname} --type headless
echo "Wait a moment for the VM to finish booting"
echo "Poweroff command (or use GUI):"
echo "vboxmanage controlvm ${vmname} poweroff"
echo "Enjoy:"
echo " ssh ${finaluser}@${finaleip}"
