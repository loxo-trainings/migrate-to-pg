$ostype="RedHat_64"
$prefix="m2pg"
$ovafile="m2pg_2019-10-15.ova"
$finalip="10.0.0.125"
$finaluser="attendee"
$vboxpath = reg query HKEY_LOCAL_MACHINE\SOFTWARE\Oracle\VirtualBox\ /v installdir|where {$_ -match "REG_SZ"}|%{$_ -replace "^.*SZ\s*",""}
$vboxmanage = $vboxpath +"VBoxManage.exe"

$isodate=get-date -f "yyyy-MM-dd"
$vmname = $prefix+"_"+$isodate

$vms = & $vboxmanage list vms |where  {$_ -match "$vmname"}
$cnt=$vms|measure-object -line
if($cnt.Lines -eq 0){
write "Importing VM"
 &$vboxmanage import  $ovafile --vsys 0 --ostype "$ostype" --vmname $vmname  --eula accept 
}
else{
write "VM already exists"
}

# Check if vm is running
$cnt = &$vboxmanage list runningvms |where {$_ -match "$vmname"}|measure-object -line
if ($cnt.Lines -gt 0){
    write "VM $vmname is running : unable to configure. Stop it and run this script."
    write "Try the following command"
    write "vboxmanage controlvm $vmname poweroff"
	return 1
}

$foundIP=Get-NetIPAddress -AddressFamily IPV4 -IPAddress 10.0.0.* -erroraction SilentlyContinue
if($foundIP){
	$foundAdapter=Get-NetAdapter -InterfaceIndex $foundIP.ifIndex -erroraction SilentlyContinue
	if($foundAdapter){
		$hostonlyif=$foundAdapter.ifDesc
	}
	else{
		Write "No Virtualbox adapter found, use manual config"
		pause
		return 2
	}
}
else{
	$hostonlyif = &$vboxmanage hostonlyif create | where {$_ -match "Interface"}|%{$_ -replace "Interface '",""}|%{$_ -replace "'.*$",""}	
}
write "Creating interface ip"
&$vboxmanage hostonlyif ipconfig "$hostonlyif" --ip 10.0.0.1 --netmask 255.255.255.0
write "Using hostonly IF: $hostonlyif"

# Bind host iface with vbox adapter 2
write "Using hostonly network $hostonlyif for $vmname"
&$vboxmanage modifyvm $vmname --nic2 hostonly
&$vboxmanage modifyvm $vmname --hostonlyadapter2 "$hostonlyif"

# Start VM and tell the user
write "Starting vm: $vmname"
&$vboxmanage startvm $vmname # --type headless
write "Wait a moment for the VM to finish booting"
write "Poweroff command (or use GUI):"
write "vboxmanage controlvm $vmname poweroff"
write "Enjoy:"
write " ssh $finaluser@$finalip"
pause
