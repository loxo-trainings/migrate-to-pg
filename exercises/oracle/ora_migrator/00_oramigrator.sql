\set ECHO 'queries'

-- General import
-- #####################################################################
-- ### OVERALL FUNCTION TO MIGRATE, JUST ONE ERROR
-- #####################################################################
\echo Global migration function called
\prompt 'Press a key' key

SELECT oracle_migrate(server => 'oradb', only_schemas => '{SAKILA}');

-- NOTICE:  Creating staging schemas "ora_stage" and "pgsql_stage" ...
-- NOTICE:  Creating Oracle metadata views in schema "ora_stage" ...
-- NOTICE:  Copying definitions to PostgreSQL staging schema "pgsql_stage" ...
-- NOTICE:  Creating schemas ...
-- NOTICE:  Creating sequences ...
-- NOTICE:  Creating foreign tables ...
-- NOTICE:  Migrating table sakila.actor ...
-- NOTICE:  Migrating table sakila.address ...
-- NOTICE:  Migrating table sakila.category ...
-- NOTICE:  Migrating table sakila.city ...
-- NOTICE:  Migrating table sakila.country ...
-- NOTICE:  Migrating table sakila.customer ...
-- NOTICE:  Migrating table sakila.film ...
-- NOTICE:  Migrating table sakila.film_actor ...
-- NOTICE:  Migrating table sakila.film_category ...
-- NOTICE:  Migrating table sakila.film_text ...
-- NOTICE:  Migrating table sakila.inventory ...
-- NOTICE:  Migrating table sakila.language ...
-- NOTICE:  Migrating table sakila.payment ...
-- NOTICE:  Migrating table sakila.rental ...
-- NOTICE:  Migrating table sakila.staff ...
-- NOTICE:  Migrating table sakila.store ...
-- WARNING:  Error creating view sakila.customer_list
-- DÉTAIL : function decode(character, integer, unknown, unknown) does not exist:
-- NOTICE:  Creating UNIQUE and PRIMARY KEY constraints ...
-- NOTICE:  Creating FOREIGN KEY constraints ...
-- NOTICE:  Creating CHECK constraints ...
-- NOTICE:  Creating indexes ...
-- NOTICE:  Setting column default values ...
-- NOTICE:  Dropping staging schemas ...
-- NOTICE:  Migration completed with 1 errors.
--  oracle_migrate
-- ----------------
--               1
-- (1 ligne)
-- #####################################################################
-- ### This sakila_global_import package is a witness
-- #####################################################################
\echo This sakila schema will be rennamed to  sakila_global_import
\prompt 'Press a key' key

ALTER SCHEMA sakila RENAME TO sakila_global_import;

-- #####################################################################
-- ### Step by step migration
-- #####################################################################
-- #####################################################################
-- ### Prepare metatdata for migration
-- #####################################################################
\echo ##################################################################
\echo  Step by step migration
\echo ##################################################################
\echo  -1- Prepare metatdata for migration
\prompt 'Press a key' key
select oracle_migrate_prepare(server=>'oradb', only_schemas=>'{SAKILA}');
-- NOTICE:  Creating staging schemas "ora_stage" and "pgsql_stage" ...
-- NOTICE:  Creating Oracle metadata views in schema "ora_stage" ...
-- NOTICE:  Copying definitions to PostgreSQL staging schema "pgsql_stage" ...
-- -[ RECORD 1 ]----------+--
-- oracle_migrate_prepare | 0

-- #####################################################################
-- ### Change some types
-- #####################################################################
\echo Look at one example table : ids are  treated as numeric should be bigint
\prompt 'Press a key' key
SELECT * FROM pgsql_stage.columns WHERE table_name = 'payment' ORDER BY position;
\echo Update ids to bigint
\prompt 'Press a key ...' key
UPDATE pgsql_stage.columns SET type_name='bigint'  where oracle_type = 'NUMBER' and column_name ~ '_id$';
\echo Check changes (columns have changed from numeric to bigint)
\prompt 'Press a key ...' key
SELECT * FROM pgsql_stage.columns WHERE table_name = 'payment' ORDER BY position;
--                                    Table distante « sakila.payment »
--    Colonne    |              Type              | Collationnement | NULL-able | Par défaut | Options FDW
-- --------------+--------------------------------+-----------------+-----------+------------+-------------
--  payment_id   | bigint                         |                 | not null  |            |
--  customer_id  | bigint                         |                 | not null  |            |
--  staff_id     | bigint                         |                 | not null  |            |
--  rental_id    | bigint                         |                 |           |            |
--  amount       | numeric(5,2)                   |                 | not null  |            |
--  payment_date | timestamp(0) without time zone |                 | not null  |            |
--  last_update  | timestamp(0) without time zone |                 | not null  |            |
-- Serveur : oradb
-- Options FDW : (schema 'SAKILA', "table" 'PAYMENT', readonly 'true', max_long '32767')
\prompt 'Press a key ...' key
-- If you plan to migrate customer.active to boolean

-- Char(1) is migrated as is, https://github.com/laurenz/oracle_fdw/tree/ORACLE_FDW_2_2_0#4-usage
-- defines a way to migrate to booleans => transform to number(1) 0 == false, anything else == true
-- If you want to do this (2 stages 1 before mkforeign, one after)
-- STAGE1
-- Just change column type in pgsql_stage.columns
-- UPDATE pgsql_stage.columns SET type_name='boolean',default_value='''t''' WHERE schema='sakila' and table_name='customer' and column_name='active'

\echo -2- Create schema, sequences and foreign tables into target schema
\prompt 'Press a key' key
select oracle_migrate_mkforeign(server=>'oradb',only_schemas=>'{SAKILA}');
-- NOTICE:  Creating schemas ...
-- NOTICE:  Creating sequences ...
-- NOTICE:  Creating foreign tables ...
--  oracle_migrate_mkforeign
-- --------------------------
--                         0

\echo Foreign tables are created as well as sequences and schemas
\d sakila.payment
\prompt 'Press a key' key



-- customer.active column
-- Char(1) is migrated as is, https://github.com/laurenz/oracle_fdw/tree/ORACLE_FDW_2_2_0#4-usage
-- defines a way to migrate to booleans => transform to number(1) 0 == false, anything else == true
-- If you want to do this 
-- STAGE 2
-- 
--ALTER FOREIGN TABLE sakila.customer OPTIONS
--   (SET table '(SELECT customer_id,store_id,first_name,last_name,email,address_id,
--                       CASE WHEN active = ''1'' THEN 1 ELSE 0 END as active,
--                       create_date,last_update
--                FROM customer)',
--    DROP schema
--   );


\echo -3- Create real tables and data
\prompt 'Press a key' key
select oracle_migrate_tables(only_schemas=>'{SAKILA}');
-- NOTICE:  Migrating table sakila.actor ...
-- NOTICE:  Migrating table sakila.address ...
-- NOTICE:  Migrating table sakila.category ...
-- NOTICE:  Migrating table sakila.city ...
-- NOTICE:  Migrating table sakila.country ...
-- NOTICE:  Migrating table sakila.customer ...
-- NOTICE:  Migrating table sakila.film ...
-- NOTICE:  Migrating table sakila.film_actor ...
-- NOTICE:  Migrating table sakila.film_category ...
-- NOTICE:  Migrating table sakila.film_text ...
-- NOTICE:  Migrating table sakila.inventory ...
-- NOTICE:  Migrating table sakila.language ...
-- NOTICE:  Migrating table sakila.payment ...
-- NOTICE:  Migrating table sakila.rental ...
-- NOTICE:  Migrating table sakila.staff ...
-- NOTICE:  Migrating table sakila.store ...
--  oracle_migrate_tables
-- -----------------------
--                      0

--
\echo Look at table definition : this really is a plain table
\prompt 'Press a key' key
\d sakila.payment
-- Tables are not Foreign Tables anymore.
-- sakila_ora_ora_migrator=> \d sakila.payment
--                                                Table "sakila.payment"
--     Column    |              Type              | Collation | Nullable |                   Default
-- --------------+--------------------------------+-----------+----------+----------------------------------------------
--  payment_id   | bigint                         |           | not null | nextval('sakila.payment_sequence'::regclass)
--  customer_id  | bigint                         |           | not null |
--  staff_id     | bigint                         |           | not null |
--  rental_id    | bigint                         |           |          |
--  amount       | numeric(5,2)                   |           | not null |
--  payment_date | timestamp(0) without time zone |           | not null |
--  last_update  | timestamp(0) without time zone |           | not null |

-- #####################################################################
-- ### PACKAGES AND FUNCTIONS
-- #####################################################################
-- We use schema name as package name
-- Migrating functionnal
\echo -4- Let us inspect functions (this one is very long)
\prompt 'Press a key' key
select * from pgsql_stage.functions;
\echo And packages
\prompt 'Press a key' key
select * from pgsql_stage.packages;
\prompt 'Press a key' key
\echo How to migrate one function in one package : customer package
\prompt 'Press a key' key
select * from pgsql_stage.packages where package_name = 'customers';
--  schema | package_name | is_body |                             source
-- --------+--------------+---------+----------------------------------------------------------------
--  sakila | customers    | f       | PACKAGE customers AS                                          +
--         |              |         |   TYPE person IS RECORD (                                     +
--         |              |         |     first_name VARCHAR2(50),                                  +
--         |              |         |     last_name VARCHAR2(50)                                    +
--         |              |         |   );                                                          +
--         |              |         |                                                               +
--         |              |         |   FUNCTION get_customer(p_customer_id NUMBER) RETURN person;  +
--         |              |         | END customers;
--  sakila | customers    | t       | PACKAGE BODY customers AS                                     +
--         |              |         |   FUNCTION get_customer(p_customer_id NUMBER) RETURN person IS+
--         |              |         |     v_person customers.person;                                +
--         |              |         |   BEGIN                                                       +
--         |              |         |     SELECT c.first_name, c.last_name                          +
--         |              |         |     INTO v_person                                             +
--         |              |         |     FROM customer c                                           +
--         |              |         |     WHERE c.customer_id = p_customer_id;                      +
--         |              |         |                                                               +
--         |              |         |     RETURN v_person;                                          +
--         |              |         |   END get_customer;                                           +
--         |              |         | END customers;
-- (2 rows)
\prompt 'Press a key' key

\echo There is one type in customers package
\echo We should migrate this type by hand.
\prompt 'Press a key' key
-- There's one type in customers package
-- We should migrate this type by hand.

-- First create schema
DROP SCHEMA IF EXISTS customers;
CREATE SCHEMA customers;

-- Then create customers.person type
CREATE TYPE customers.person AS (first_name varchar(50), last_name varchar(50));
INSERT INTO  pgsql_stage.functions VALUES ('customers','get_customer','false',$PGSQL$
FUNCTION get_customer(p_customer_id bigint) RETURNS customers.person AS $PROC$
DECLARE
v_person customers.person;
BEGIN
   SELECT c.first_name, c.last_name
   INTO v_person
   FROM sakila.customer c
   WHERE c.customer_id = p_customer_id;

   RETURN v_person;
 END
 $PROC$
 LANGUAGE plpgsql;
$PGSQL$, $ORCL$

-- Then create function
FUNCTION get_customer(p_customer_id NUMBER) RETURN person IS
    v_person customers.person;
  BEGIN
    SELECT c.first_name, c.last_name
    INTO v_person
    FROM customer c
    WHERE c.customer_id = p_customer_id;

    RETURN v_person;
  END get_customer;
$ORCL$, 'true','false');


\echo List rentals package entries
\echo Look closer : there is types there
\prompt  'Press a key...' key

-- List rentals package entries
-- Look closer : there's types there
select * from pgsql_stage.packages where package_name='rentals';
--Ora_migrator does not provide type migration, this should be manually done
\echo Ora_migrator does not provide type migration, this should be manually done
\prompt  'Press a key...' key
-- we need to solve types.
set search_path to sakila,public,rentals,customers;

CREATE TYPE  SAKILA.COUNTRY_T AS (COUNTRY_ID SMALLINT,COUNTRY VARCHAR(50),LAST_UPDATE DATE);
CREATE TYPE  SAKILA.LANGUAGE_T AS (LANGUAGE_ID SMALLINT,NAME CHAR(20),LAST_UPDATE DATE);
CREATE TYPE  SAKILA.CITY_T AS (CITY_ID INTEGER,CITY VARCHAR(50),COUNTRY COUNTRY_T,LAST_UPDATE DATE);
CREATE TYPE  SAKILA.FILM_T AS (FILM_ID INTEGER,TITLE VARCHAR(255),DESCRIPTION text,RELEASE_YEAR VARCHAR(4),LANGUAGE LANGUAGE_T,ORIGINAL_LANGUAGE LANGUAGE_T,RENTAL_DURATION SMALLINT,RENTAL_RATE DECIMAL(4,2),LENGTH SMALLINT,REPLACEMENT_COST DECIMAL(5,2),RATING VARCHAR(10),SPECIAL_FEATURES VARCHAR(100),LAST_UPDATE DATE);
CREATE TYPE  SAKILA.ACTOR_T AS (ACTOR_ID DECIMAL,FIRST_NAME VARCHAR(45),LAST_NAME VARCHAR(45),LAST_UPDATE DATE);
CREATE TYPE  SAKILA.ADDRESS_T AS (ADDRESS_ID INTEGER,ADDRESS VARCHAR(50),ADDRESS2 VARCHAR(50),DISTRICT VARCHAR(20),CITY CITY_T,POSTAL_CODE VARCHAR(10),PHONE VARCHAR(20),LAST_UPDATE DATE);
CREATE TYPE  SAKILA.CATEGORY_T AS (CATEGORY_ID SMALLINT,NAME VARCHAR(25),LAST_UPDATE DATE);
CREATE TYPE  SAKILA.CUSTOMER_T AS (CUSTOMER_ID INTEGER,FIRST_NAME VARCHAR(45),LAST_NAME VARCHAR(45),EMAIL VARCHAR(50),ADDRESS ADDRESS_T,ACTIVE CHAR(1),CREATE_DATE DATE,LAST_UPDATE DATE);
--Manage sets and arrays types
CREATE TYPE  SAKILA.FILM_INFO_T AS (FILM FILM_T,ACTORS ACTOR_T[],CATEGORIES CATEGORY_T[]);
CREATE TYPE  SAKILA.CUSTOMER_RENTAL_HISTORY_T AS (CUSTOMER CUSTOMER_T,FILMS FILM_T[]);

-- Don't put all data by hand
-- Split package body into many functions with one function (thanks PG!)
\echo Do not put all data by hand
\echo Split package body into many functions with one function (thanks PG!)
\prompt  'Press a key...' key
WITH fn AS (
    SELECT
        schema
        ,package_name
        , regexp_matches(source,'(FUNCTION ([A-Za-z0-9_]+?)(?:[\( ]).+?END (?:[A-Za-z0-9_]+?);)','g') as func
    FROM
        pgsql_stage.packages
    WHERE
        is_body='t'
        and package_name='rentals'
),
func_content as(
    select
         fn.package_name as schema
        , lower(fn.func[2]||
            CASE
                WHEN row_number() over(partition by schema,package_name,fn.func[2]) <> 1 THEN
                    '_'||row_number() over (partition by schema, package_name, fn.func[2])
                ELSE
                    ''
                END) as function_name
        , fn.func[1] source
    from fn
)
INSERT INTO
    pgsql_stage.functions(schema, function_name,is_procedure,source,oracle_source,migrate,verified)
    SELECT  schema, function_name,'f',source,source,'f','f'
    FROM  func_content;


--- Then update functions definition to translate to PostgreSQL (and editor is better thant doing this way but this is a script)
\echo Then update functions definition to translate to PostgreSQL (and editor is better thant doing this way but this is a script)
\echo this part is very long, read it while quietly seated at your office.
\prompt 'Press a key...' key
\echo Really!
\prompt 'Press a key...' key
UPDATE pgsql_stage.functions SET source=$DEF$
FUNCTION get_actors() RETURNS SETOF sakila.ACTOR_T AS $BODY$
select (a.actor_id,a.first_name,a.last_name,a.last_update)::sakila.actor_t FROM actor a;
$BODY$
LANGUAGE SQL;
$DEF$ , migrate='t'
WHERE schema='rentals' AND function_name='get_actors';

UPDATE pgsql_stage.functions SET source=$DEF$
FUNCTION get_actor(p_actor_id integer) RETURNS  sakila.ACTOR_T AS $BODY$
select (a.actor_id,a.first_name,a.last_name,a.last_update)::sakila.actor_t FROM actor a where a.actor_id = p_actor_id;
$BODY$
LANGUAGE SQL;
$DEF$, migrate='t'
WHERE schema='rentals' AND function_name='get_actor';

UPDATE pgsql_stage.functions SET source=$DEF$
FUNCTION get_customer(p_customer_id integer) RETURNS  sakila.CUSTOMER_T AS $BODY$
    select
        (
             c.customer_id
            ,c.first_name
            ,c.last_name
            ,c.email,
            (
                a.address_id,
                a.address,
                a.address2,
                a.district,
                (
                    i.city_id,
                    i.city,
                    (
                        o.country_id,
                        o.country,
                        o.last_update
                    )::sakila.country_t,
                    i.last_update
                )::sakila.city_t,
                a.postal_code,
                a.phone,
                a.last_update
            )::sakila.address_t
            ,c.active
            ,c.create_date
            ,c.last_update
        )::sakila.customer_t
     FROM customer c
     LEFT JOIN address a USING(address_id)
     LEFT JOIN city i USING(city_id)
     LEFT JOIN country o USING(country_id)
     where c.customer_id = p_customer_id
$BODY$
LANGUAGE SQL;
$DEF$, migrate='t'
WHERE schema='rentals' AND function_name='get_customer';

UPDATE pgsql_stage.functions SET source=$DEF$
FUNCTION get_customers() RETURNS SETOF  sakila.CUSTOMER_T AS $BODY$
    select
        (
             c.customer_id
            ,c.first_name
            ,c.last_name
            ,c.email,
            (
                a.address_id,
                a.address,
                a.address2,
                a.district,
                (
                    i.city_id,
                    i.city,
                    (
                        o.country_id,
                        o.country,
                        o.last_update
                    )::sakila.country_t,
                    i.last_update
                )::sakila.city_t,
                a.postal_code,
                a.phone,
                a.last_update
            )::sakila.address_t
            ,c.active
            ,c.create_date
            ,c.last_update
        )::sakila.customer_t
     FROM customer c
     LEFT JOIN address a USING(address_id)
     LEFT JOIN city i USING(city_id)
     LEFT JOIN country o USING(country_id)
$BODY$
LANGUAGE SQL;
$DEF$, migrate='t'
WHERE schema='rentals' AND function_name='get_customers';

UPDATE pgsql_stage.functions SET source=$DEF$
FUNCTION get_film(p_film_id integer) RETURNS  sakila.FILM_T AS $BODY$
    SELECT (
      f.film_id,
      f.title,
      f.description,
      f.release_year,
      CASE WHEN l1.language_id IS NOT NULL THEN
        (
          l1.language_id,
          l1.name,
          l1.last_update
        )::sakila.language_t
        ELSE
            NULL
      END,
      CASE WHEN l2.language_id IS NOT NULL THEN
        (
          l2.language_id,
          l2.name,
          l2.last_update
        )::sakila.language_t
        ELSE
            NULL
        END,
      f.rental_duration,
      f.rental_rate,
      f.length,
      f.replacement_cost,
      f.rating,
      f.special_features,
      f.last_update
    )::sakila.film_t
    FROM film f
    LEFT JOIN language l1 ON f.language_id = l1.language_id
    LEFT JOIN language l2 ON f.original_language_id = l2.language_id
    WHERE f.film_id = p_film_id;
$BODY$
LANGUAGE SQL;
$DEF$, migrate='t'
WHERE schema='rentals' AND function_name='get_film';


UPDATE pgsql_stage.functions SET source=$DEF$
FUNCTION get_films() RETURNS SETOF sakila.FILM_T AS $BODY$
    SELECT (
      f.film_id,
      f.title,
      f.description,
      f.release_year,
      CASE WHEN l1.language_id IS NOT NULL THEN
        (
          l1.language_id,
          l1.name,
          l1.last_update
        )::sakila.language_t
        ELSE
            NULL
      END,
      CASE WHEN l2.language_id IS NOT NULL THEN
        (
          l2.language_id,
          l2.name,
          l2.last_update
        )::sakila.language_t
        ELSE
            NULL
        END,
      f.rental_duration,
      f.rental_rate,
      f.length,
      f.replacement_cost,
      f.rating,
      f.special_features,
      f.last_update
    )::sakila.film_t
    FROM film f
    LEFT JOIN language l1 ON f.language_id = l1.language_id
    LEFT JOIN language l2 ON f.original_language_id = l2.language_id
$BODY$
LANGUAGE SQL;
$DEF$, migrate='t'
WHERE schema='rentals' AND function_name='get_films';


UPDATE pgsql_stage.functions SET source=$DEF$
FUNCTION get_customer_rental_history(p_customer sakila.customer_t) RETURNS sakila.CUSTOMER_RENTAL_HISTORY_T AS $BODY$
    SELECT p_customer, array( select get_film(f.film_id::integer)
    FROM (
      SELECT DISTINCT f.film_id
      FROM film f
      LEFT JOIN inventory i ON i.film_id = f.film_id
      LEFT JOIN rental r ON r.inventory_id = i.inventory_id
      WHERE r.customer_id = p_customer.customer_id
    ) f
    ORDER BY f.film_id);

$BODY$
LANGUAGE SQL;
$DEF$, migrate='t'
WHERE schema='rentals' AND function_name='get_customer_rental_history';

UPDATE pgsql_stage.functions SET source=$DEF$
FUNCTION get_customer_rental_history(p_customer_id integer) RETURNS sakila.CUSTOMER_RENTAL_HISTORY_T AS $BODY$
    SELECT get_customer_rental_history(  get_customer(p_customer_id::integer));
$BODY$
LANGUAGE SQL;
$DEF$, migrate='t'
WHERE schema='rentals' AND function_name='get_customer_rental_history_2';

UPDATE pgsql_stage.functions SET source=$DEF$
FUNCTION get_film_info(p_film sakila.film_t) RETURNS sakila.FILM_INFO_T AS $BODY$
     SELECT (
      p_film,
      array(SELECT (
                a.actor_id,
                a.first_name,
                a.last_name,
                a.last_update
                )::sakila.actor_t
                FROM actor a
                JOIN film_actor fa ON fa.actor_id = a.actor_id
                WHERE fa.film_id = p_film.film_id
                ORDER BY a.actor_id
        ),
      array(SELECT (
                c.category_id,
                c.name,
                c.last_update
                )::CATEGORY_T
                FROM category c
                JOIN film_category fc ON fc.category_id = c.category_id
                WHERE fc.film_id = p_film.film_id
                ORDER BY c.category_id)
     )::FILM_INFO_T;
$BODY$
LANGUAGE SQL;
$DEF$, migrate='t'
WHERE schema='rentals' AND function_name='get_film_info';

UPDATE pgsql_stage.functions SET source=$DEF$
FUNCTION get_film_info(p_film_id integer) RETURNS sakila.FILM_INFO_T AS $BODY$
     SELECT GET_FILM_INFO(GET_FILM(p_film_id));
$BODY$
LANGUAGE SQL;
$DEF$, migrate='t'
WHERE schema='rentals' AND function_name='get_film_info_2';

-- No types for rentals schema
-- Just drop if exists
-- 
\echo Drop rentals schema as there is no type in it.
\prompt 'Press a key...' key
DROP SCHEMA IF EXISTS rentals CASCADE;
CREATE SCHEMA rentals;
-- Only function, we created the type before
\echo For customers schema, only drop the function
\prompt 'Press a key ...' key
DROP FUNCTION IF EXISTS customers.get_customer;

\echo Then migrate functions  
-- migrate functions
select oracle_migrate_functions(only_schemas=>'{rentals,customers}');
set search_path to rentals,sakila,public

-- #####################################################################
-- ### Sequences
-- #####################################################################

-- Sequences are not attached
\echo -5- Migrate sequences 
\echo  First, update columns default using sequence name
\prompt 'Press a key ...' key
with seq as (
        select
            nspname||'.'||relname as seqname,
            nspname||'.'||(regexp_split_to_array(relname,'_sequence'))[1] as rel ,
            (regexp_split_to_array(relname,'_sequence'))[1] as col
        from pg_class join pg_namespace on pg_class.relnamespace = pg_namespace.oid
        where
            relkind='S' and nspname='sakila'
)
SELECT 'alter table '||rel||' ALTER COLUMN '||col||'_id SET DEFAULT nextval('''||seqname||''');'
as query FROM seq;

\gexec

\echo Check sequences values from ORacle and PostgreSQL
\prompt 'Press a key ...' key
-- lets check each sequence values
select * from pgsql_stage.sequences ;
select * from ora_stage.sequences ;

\echo  Sequence are not really attached to its table in pg_catalog so we have to rely on naming
\echo  Attaching
\prompt 'Press a key ...' key
 WITH dat as (
    SELECT
          relname as seqname
        , (regexp_match(relname,e'(.+)_sequence'))[1] as relname
        , nspname
    FROM  pg_class c
    join pg_namespace n on c.relnamespace = n.oid
    where
        relkind='S' and n.nspname='sakila'
)
select
    'ALTER SEQUENCE '||nspname||'.'||seqname||' OWNED BY '||nspname||'.'||relname||'.'||relname||'_id;'
from dat \gexec

\echo Put sequence currval to correct value
\prompt 'Press a key ...' key
-- Updating sequence values
SELECT 'SELECT setval('''||seq.sequence_name||''', max("'||a.attname||'")::bigint) FROM "'||d.refobjid::regclass||'";'
FROM   pg_depend    d
JOIN   pg_attribute a ON a.attrelid = d.refobjid
  AND a.attnum   = d.refobjsubid
JOIN  pg_class as pg_class_seq on pg_class_seq.relname::regclass = d.objid and pg_class_seq.relkind = 'S'
JOIN  information_schema.sequences seq on seq.sequence_schema = 'sakila' and pg_class_seq.relname = seq.sequence_name
WHERE
1=1
AND d.refobjsubid > 0
AND    d.classid = 'pg_class'::regclass \gexec

-- #####################################################################
-- ### Triggers
-- #####################################################################
\echo -6- Migrate triggers
\echo First inspect triggers codes 
\prompt 'Press a key ...' key
-- Lets inspect triggers
select schema,table_name,triggering_event,trigger_body from pgsql_stage.triggers where trigger_name ~'.*update$';
\prompt 'Press a key ...' key
select schema,table_name,triggering_event,trigger_body from pgsql_stage.triggers where trigger_name !~'.*update$';
-- It's only last_update column update with current_date!
-- lets set it to PG style
\echo There is a rework 
\echo Sequences values are handled by sequences and column defaults
\echo So Trigger only update last_update column
\echo Therefore there is only one  trigger code : this one
\prompt 'Press a key ...' key
UPDATE pgsql_stage.triggers set migrate = 't', trigger_body=$BODY$
BEGIN
NEW.last_update:=current_date;
RETURN NEW;
END;
$BODY$;

\echo And finally migrate the triggers
select oracle_migrate_triggers(only_schemas=>'{SAKILA}');

-- #####################################################################
-- ### Views
-- #####################################################################
\echo -8- Views
\echo First inspect views : only one view should be migrated: customer_list
\prompt 'Press a key ...' key
-- Remember the view problem in ora2pg : customer_list
select schema,view_name,definition, migrate, verified from pgsql_stage.views WHERE view_name = 'customer_list';
--update pgsql_stage.views SET definition = regexp_replace(definition,e', 1,',', ''1'',') where view_name = 'customer_list';
\echo Update the view query to fit our needds
\prompt 'Press a key ...' key
update pgsql_stage.views SET definition = $VIEW$
    SELECT cu.customer_id AS ID,
        cu.first_name||' '||cu.last_name AS name,
        a.address AS address,
        a.postal_code AS zip_code,
        a.phone AS phone,
        city.city AS city,
        country.country AS country,
        CASE WHEN (cu.active = '1' ) THEN 'active' ELSE '' END AS notes,
        cu.store_id AS SID
    FROM sakila.customer cu JOIN sakila.address a ON cu.address_id = a.address_id JOIN sakila.city ON a.city_id = city.city_id
        JOIN sakila.country ON city.country_id = country.country_id
$VIEW$ where view_name='customer_list';
\echo Really migrate view
\prompt 'Press a key ...' key
select oracle_migrate_views(only_schemas=>'{SAKILA}');

-- TODO test migration

\echo  -9- Cleanup the migration... (remove staging schemas)
\prompt 'Press a key ...' key
select oracle_migrate_finish();
