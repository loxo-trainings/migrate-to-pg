[1] TABLE ACTOR (owner: SAKILA, 200 rows)
[2] TABLE ADDRESS (owner: SAKILA, 603 rows)
[3] TABLE CATEGORY (owner: SAKILA, 16 rows)
[4] TABLE CITY (owner: SAKILA, 600 rows)
[5] TABLE COUNTRY (owner: SAKILA, 109 rows)
[6] TABLE CUSTOMER (owner: SAKILA, 599 rows)
[7] TABLE FILM (owner: SAKILA, 1000 rows)
[8] TABLE FILM_ACTOR (owner: SAKILA, 5462 rows)
[9] TABLE FILM_CATEGORY (owner: SAKILA, 1000 rows)
[10] TABLE FILM_TEXT (owner: SAKILA, 0 rows)
[11] TABLE INVENTORY (owner: SAKILA, 4581 rows)
[12] TABLE LANGUAGE (owner: SAKILA, 6 rows)
[13] TABLE PAYMENT (owner: SAKILA, 16049 rows)
[14] TABLE RENTAL (owner: SAKILA, 16044 rows)
[15] TABLE STAFF (owner: SAKILA, 2 rows)
[16] TABLE STORE (owner: SAKILA, 2 rows)
----------------------------------------------------------
Total number of rows: 46273

Top 10 of tables sorted by number of rows:
	[1] TABLE PAYMENT has 16049 rows
	[2] TABLE RENTAL has 16044 rows
	[3] TABLE FILM_ACTOR has 5462 rows
	[4] TABLE INVENTORY has 4581 rows
	[5] TABLE FILM_CATEGORY has 1000 rows
	[6] TABLE FILM has 1000 rows
	[7] TABLE ADDRESS has 603 rows
	[8] TABLE CITY has 600 rows
	[9] TABLE CUSTOMER has 599 rows
	[10] TABLE ACTOR has 200 rows
Top 10 of largest tables:
	[1] TABLE COUNTRY: 0 MB (109 rows)
	[2] TABLE LANGUAGE: 0 MB (6 rows)
	[3] TABLE CITY: 0 MB (600 rows)
	[4] TABLE STAFF: 0 MB (2 rows)
	[5] TABLE CATEGORY: 0 MB (16 rows)
	[6] TABLE ACTOR: 0 MB (200 rows)
	[7] TABLE STORE: 0 MB (2 rows)
	[8] TABLE ADDRESS: 0 MB (603 rows)
	[9] TABLE FILM_CATEGORY: 0 MB (1000 rows)
	[10] TABLE CUSTOMER: 0 MB (599 rows)
