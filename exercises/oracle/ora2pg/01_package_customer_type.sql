CREATE TYPE customers.person AS (
    first_name VARCHAR(50),
    last_name VARCHAR(50)
  );
