#!/bin/bash

# This shell script migrate oracle sakila DB with both basic et better configuration in two different target bases

MYCWD=$(pwd)
for CDB in "sakila_ora_ora2pg" "sakila_ora_ora2pg_better"; do 
    echo "********************************************************"
    echo "*      ${CDB} Migration                        "
    echo "********************************************************"
    cd ${HOME}/${CDB}
    ${HOME}/exercises/oracle/ora2pg/00_initialize_migration.sh ${CDB}
done
cd ${MYCWD}