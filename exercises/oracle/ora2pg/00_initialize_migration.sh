#!/bin/bash
TARGETDB="$1"
## Set NOWAIT to not emptystring to test all without any pause.
# ora2pg import will say yes to all questions.
if [ ! "${NOWAIT}0" = "0" ]; then
    export O2POPTS="-y"
    export NOPAUSE="1"
else
    export O2POPTS=""
    export NOPAUSE=""
fi
echo "${O2POPTS} => ora2pg opts, ${NOPAUSE} => nopause"
function format_echo ()
{
    echo -e  "*************************
$*
*************************"
}

function pause()
{
    format_echo $*
    if [ "${NOPAUSE}0" = "0" ]; then 
        read -p "press Enter ..."
    fi
}


echo  "Run import for table, sequene type and  view"

pause "./import_all.sh -s -d ${TARGETDB} ${O2POPTS} -j 2 -o attendee -t TABLE,SEQUENCE,TYPE,VIEW -x -f
"
./import_all.sh -s -d ${TARGETDB} ${O2POPTS} -j 2 -o attendee -t TABLE,SEQUENCE,TYPE,VIEW -x -f
format_echo  "Fails on view"
pause "custommer.active column is now a boolean and no longuer a char(1) Y/N 1/0 data. => Change view code source"

if [ $TARGETDB -eq "sakila_ora_ora2pg_bg" ]; then
    sed -i 's/cu.active *= *1/cu.active/g' schema/views/CUSTOMER_LIST_view.sql 
else
    sed -i 's/cu.active *= *1/cu.active = '"'1'"'/g' schema/views/CUSTOMER_LIST_view.sql 
fi
cat schema/views/CUSTOMER_LIST_view.sql

echo "Now reimport view"
pause "./import_all.sh -s ${O2POPTS} -d ${TARGETDB} -j 2 -o attendee -t VIEW -x -f"
# Once fixed, replay views.
./import_all.sh -s -d ${TARGETDB}  ${O2POPTS} -j 2 -o attendee -t VIEW -x -f

echo "There's a \"bug\" in ora2pg : it does not migrate package types and variables 
We add type creation in schema for packages.

sed -i '/CREATE SCHEMA customers;/a\\\i \/home\/attendee\/exercises\/oracle\/ora2pg\/01_package_customer_type.sql' schema/packages/package.sql

# Adjust output type for function
sed -i 's/RETURNS PERSON/RETURNS customers.person/g' schema/packages/customers/get_customer_package.sql
"
pause ""
sed -i '/CREATE SCHEMA customers;/a\\\i \/home\/attendee\/exercises\/oracle\/ora2pg\/01_package_customer_type.sql' schema/packages/package.sql
# Adjust output type for function
sed -i 's/RETURNS PERSON/RETURNS customers.person/g' schema/packages/customers/get_customer_package.sql

echo "Import package, in this version rental will fail"
pause "./import_all.sh   ${O2POPTS}  -d ${TARGETDB} -j 2 -P 2 -o attendee -t PACKAGE -f -x
"
# Will crash for rentals because of BULK COLLECT INTO should be SET OF
./import_all.sh  ${O2POPTS}  -d ${TARGETDB} -j 2 -P 2 -o attendee -t PACKAGE -f -x

pause "Manually import customers package"
echo "CREATE SCHEMA IF NOT EXISTS customers;
\i /home/attendee/exercises/oracle/ora2pg/01_package_customer_type.sql
\i schema/packages/customers/get_customer_package.sql" | psql -e -U attendee -d ${TARGETDB}
# psql -U attendee -d ${TARGETDB} -f  /home/attendee/exercises/oracle/ora2pg/01_package_customer_type.sql
# psql -U attendee -d ${TARGETDB} -f  schema/packages/customers/get_customer_package.sql


echo "Import data"
pause "./import_all.sh   ${O2POPTS}  -d ${TARGETDB} -j 2 -P 2 -o attendee -a"
# Then import data
./import_all.sh  ${O2POPTS}  -d ${TARGETDB} -j 2 -P 2 -o attendee -a

echo "Then import triggers"
pause "./import_all.sh -s ${O2POPTS}  -d ${TARGETDB} -j 2 -o attendee -t TRIGGER -f"

# Add trigger index constraints fk
./import_all.sh -s  ${O2POPTS}  -d ${TARGETDB} -j 2 -o attendee -t TRIGGER -f

echo "Let's control : only functions should be missing"
pause "ora2pg -t TEST -c config/ora2pg.conf"
ora2pg -t TEST ${O2POPTS} -c config/ora2pg.conf|tee migration_report.txt