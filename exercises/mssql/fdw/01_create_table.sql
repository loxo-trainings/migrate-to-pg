CREATE SCHEMA IF NOT EXISTS sakila;
ALTER SCHEMA sakila OWNER TO ATTENDEE;
GRANT USAGE ON SCHEMA sakila TO attendee;
SET search_path to sakila, public, "$user";
ALTER DATABASE sakila_mssql_tds_fdw SET search_path to  sakila,foreign_sakila ,public, "$user";

SELECT 'CREATE TABLE sakila.'|| table_name || ' AS SELECT * FROM foreign_sakila.'||table_name
  FROM  information_schema.tables
  WHERE table_schema = 'foreign_sakila' and table_type = 'FOREIGN' and table_name not in (
  'actor_info', 'customer_list', 'film_list', 'nicer_but_slower_film_list', 'sales_by_film_category', 'sales_by_store', 'staff_list'
)\gexec