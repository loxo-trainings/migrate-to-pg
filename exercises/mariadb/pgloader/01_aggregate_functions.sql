CREATE FUNCTION _group_concat(text, text) RETURNS text
    LANGUAGE sql IMMUTABLE
    AS $BODY$
SELECT CASE
  WHEN $2 IS NULL THEN $1
  WHEN $1 IS NULL THEN $2
  ELSE $1 || ', ' || $2
END
$BODY$;


ALTER FUNCTION _group_concat(text, text) OWNER TO attendee;



CREATE FUNCTION get_customer_balance(p_customer_id integer, p_effective_date timestamp with time zone) RETURNS numeric
    LANGUAGE plpgsql
    AS $BODY$
DECLARE
    v_rentfees DECIMAL(5,2);
    v_overfees INTEGER;
    v_payments DECIMAL(5,2);
BEGIN
    SELECT COALESCE(SUM(film.rental_rate),0) INTO v_rentfees
    FROM film, inventory, rental
    WHERE film.film_id = inventory.film_id
      AND inventory.inventory_id = rental.inventory_id
      AND rental.rental_date <= p_effective_date
      AND rental.customer_id = p_customer_id;

    SELECT COALESCE(SUM(IF((rental.return_date - rental.rental_date) > (film.rental_duration * '1 day'::interval),
        ((rental.return_date - rental.rental_date) - (film.rental_duration * '1 day'::interval)),0)),0) INTO v_overfees
    FROM rental, inventory, film
    WHERE film.film_id = inventory.film_id
      AND inventory.inventory_id = rental.inventory_id
      AND rental.rental_date <= p_effective_date
      AND rental.customer_id = p_customer_id;

    SELECT COALESCE(SUM(payment.amount),0) INTO v_payments
    FROM payment
    WHERE payment.payment_date <= p_effective_date
    AND payment.customer_id = p_customer_id;

    RETURN v_rentfees + v_overfees - v_payments;
END
$BODY$;


ALTER FUNCTION get_customer_balance(p_customer_id integer, p_effective_date timestamp with time zone) OWNER TO attendee;



CREATE FUNCTION inventory_held_by_customer(p_inventory_id integer) RETURNS integer
    LANGUAGE plpgsql
    AS $BODY$
DECLARE
    v_customer_id INTEGER;
BEGIN

  SELECT customer_id INTO v_customer_id
  FROM rental
  WHERE return_date IS NULL
  AND inventory_id = p_inventory_id;

  RETURN v_customer_id;
END $BODY$;


ALTER FUNCTION inventory_held_by_customer(p_inventory_id integer) OWNER TO attendee;



CREATE FUNCTION inventory_in_stock(p_inventory_id integer) RETURNS boolean
    LANGUAGE plpgsql
    AS $BODY$
DECLARE
    v_rentals INTEGER;
    v_out     INTEGER;
BEGIN

    SELECT count(*) INTO v_rentals
    FROM rental
    WHERE inventory_id = p_inventory_id;

    IF v_rentals = 0 THEN
      RETURN TRUE;
    END IF;

    SELECT COUNT(rental_id) INTO v_out
    FROM inventory LEFT JOIN rental USING(inventory_id)
    WHERE inventory.inventory_id = p_inventory_id
    AND rental.return_date IS NULL;

    IF v_out > 0 THEN
      RETURN FALSE;
    ELSE
      RETURN TRUE;
    END IF;
END $BODY$;


ALTER FUNCTION inventory_in_stock(p_inventory_id integer) OWNER TO attendee;

CREATE FUNCTION last_day(timestamp with time zone) RETURNS date
    LANGUAGE sql IMMUTABLE STRICT
    AS $BODY$
  SELECT CASE
    WHEN EXTRACT(MONTH FROM $1) = 12 THEN
      (((EXTRACT(YEAR FROM $1) + 1) operator(pg_catalog.||) '-01-01')::date - INTERVAL '1 day')::date
    ELSE
      ((EXTRACT(YEAR FROM $1) operator(pg_catalog.||) '-' operator(pg_catalog.||) (EXTRACT(MONTH FROM $1) + 1) operator(pg_catalog.||) '-01')::date - INTERVAL '1 day')::date
    END
$BODY$;


ALTER FUNCTION last_day(timestamp with time zone) OWNER TO attendee;

CREATE OR REPLACE FUNCTION fn_active_trigger_schema(DoEnable boolean, triggerSchemaName text) RETURNS integer AS
$BODY$
DECLARE
mytables RECORD;
BEGIN
  FOR mytables in  SELECT trig.tgname as triggername, trig_table.relname as tablename
        FROM pg_trigger trig
        INNER JOIN pg_class trig_table
        ON trig.tgrelid = trig_table.oid
        INNER JOIN  pg_namespace schem on trig_table.relnamespace = schem.oid
        WHERE tgisinternal = false and schem.nspname = triggerSchemaName
  LOOP
    IF DoEnable THEN
      EXECUTE 'ALTER TABLE ' || mytables.tablename || ' ENABLE TRIGGER ' || mytables.triggername;
      RAISE NOTICE 'ENABLE trigger % on table %',  mytables.triggername, mytables.tablename;
    ELSE
      EXECUTE 'ALTER TABLE ' || mytables.tablename || ' DISABLE TRIGGER ' || mytables.triggername;
      RAISE NOTICE 'DISABLE trigger % on table %',  mytables.triggername, mytables.tablename;

    END IF;
  END LOOP;
  RETURN 1;
END;
$BODY$
LANGUAGE 'plpgsql' VOLATILE;

ALTER FUNCTION fn_active_trigger_schema(DoEnable boolean, triggerSchemaName text) OWNER TO attendee;

CREATE AGGREGATE group_concat(text) (
    SFUNC = _group_concat,
    STYPE = text
);

ALTER AGGREGATE group_concat(text) OWNER TO attendee;

CREATE FUNCTION rewards_report(min_monthly_purchases integer, min_dollar_amount_purchased numeric) RETURNS SETOF customer
    LANGUAGE plpgsql SECURITY DEFINER
AS $$
DECLARE
    last_month_start DATE;
    last_month_end DATE;
rr RECORD;
tmpSQL TEXT;
BEGIN

    IF min_monthly_purchases = 0 THEN
        RAISE EXCEPTION 'Minimum monthly purchases parameter must be > 0';
    END IF;
    IF min_dollar_amount_purchased = 0.00 THEN
        RAISE EXCEPTION 'Minimum monthly dollar amount purchased parameter must be > 0.00';
    END IF;

    last_month_start := CURRENT_DATE - '3 month'::interval;
    last_month_start := to_date((extract(YEAR FROM last_month_start) || '-' || extract(MONTH FROM last_month_start) || '-01'),'YYYY-MM-DD');
    last_month_end := LAST_DAY(last_month_start);

    CREATE TEMPORARY TABLE tmpCustomer (customer_id INTEGER NOT NULL PRIMARY KEY);

    tmpSQL := 'INSERT INTO tmpCustomer (customer_id) SELECT p.customer_id FROM payment AS p WHERE DATE(p.payment_date) BETWEEN '||quote_literal(last_month_start) ||' AND '|| quote_l(last_month_end) || ' GROUP BY customer_id HAVING SUM(p.amount) > '|| min_dollar_amount_purchased || '   AND COUNT(customer_id) > ' ||min_monthly_purchases ;

    EXECUTE tmpSQL;

    FOR rr IN EXECUTE 'SELECT c.* FROM tmpCustomer AS t INNER JOIN customer AS c ON t.customer_id = c.customer_id' LOOP
        RETURN NEXT rr;
    END LOOP;
    tmpSQL := 'DROP TABLE tmpCustomer';
    EXECUTE tmpSQL;
 RETURN;
END
$$;

ALTER FUNCTION rewards_report(min_monthly_purchases integer, min_dollar_amount_purchased numeric) OWNER TO attendee;
