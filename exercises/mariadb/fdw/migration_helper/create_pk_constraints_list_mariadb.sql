SELECT
   'ALTER TABLE ONLY ' ||  "TABLE_NAME"|| ' ADD CONSTRAINT ' ||"TABLE_NAME"||'_pkey PRIMARY KEY ('||string_agg ("COLUMN_NAME",',')||');'
FROM
    foreign_information_schema."STATISTICS"
WHERE
    "TABLE_SCHEMA" = 'sakila'
     AND
    "INDEX_NAME" = 'PRIMARY'
GROUP BY    "INDEX_NAME" , "TABLE_NAME";





