with mariadb_fk as (
     select
     (fks."CONSTRAINT_SCHEMA" ||'.'|| fks."TABLE_NAME") as foreign_table,
     kcu."COLUMN_NAME" as foreign_column,
     (fks."UNIQUE_CONSTRAINT_SCHEMA"|| '.'|| fks."REFERENCED_TABLE_NAME") as primary_table,
     kcu."REFERENCED_COLUMN_NAME" as primary_column,
     fks."CONSTRAINT_NAME" as constraint_name
from foreign_information_schema."REFERENTIAL_CONSTRAINTS" fks
join foreign_information_schema."KEY_COLUMN_USAGE" kcu
     on fks."CONSTRAINT_SCHEMA" = kcu."TABLE_SCHEMA"
     and fks."TABLE_NAME" = kcu."TABLE_NAME"
     and fks."CONSTRAINT_NAME" = kcu."CONSTRAINT_NAME"
group by fks."CONSTRAINT_SCHEMA",
         fks."TABLE_NAME",
         fks."UNIQUE_CONSTRAINT_SCHEMA",
         fks."REFERENCED_TABLE_NAME",
         fks."CONSTRAINT_NAME",
         kcu."COLUMN_NAME",
         kcu."REFERENCED_COLUMN_NAME"
order by fks."CONSTRAINT_SCHEMA",
         fks."TABLE_NAME"
)
select 'ALTER TABLE ONLY ' || foreign_table || ' ADD CONSTRAINT ' || constraint_name || ' FOREIGN KEY ( '|| foreign_column ||' ) REFERENCES '|| primary_table ||'('||primary_column ||') ON UPDATE CASCADE ON DELETE RESTRICT;'   from mariadb_fk;

-- ALTER TABLE ONLY store ADD CONSTRAINT store_address_id_fkey FOREIGN KEY (address_id) REFERENCES address(address_id) ON UPDATE CASCADE ON DELETE RESTRICT;
