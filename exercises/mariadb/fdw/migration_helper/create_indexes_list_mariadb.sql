
-- Beware We convert all mariadb indexes to btree even the other types (fulltext for example.)
--  It's
SELECT
        'CREATE INDEX '|| "INDEX_NAME" ||'_'|| "TABLE_NAME"||' ON '||"TABLE_NAME"|| ' USING btree ('||string_agg ("COLUMN_NAME",',')||');'
FROM
    foreign_information_schema."STATISTICS"

WHERE
    "TABLE_SCHEMA" = 'sakila'
     AND
    "INDEX_NAME" <> 'PRIMARY'
GROUP BY    "INDEX_NAME" , "TABLE_NAME";





