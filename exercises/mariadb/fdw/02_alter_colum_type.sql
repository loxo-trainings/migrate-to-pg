ALTER TABLE customer ALTER COLUMN active TYPE BOOLEAN USING active::integer::BOOLEAN;
ALTER TABLE staff ALTER COLUMN active TYPE BOOLEAN USING active::integer::BOOLEAN;


DO $$BEGIN IF NOT EXISTS (SELECT 1 FROM pg_catalog.pg_type WHERE typname = 'rating_t') THEN CREATE TYPE rating_t AS enum('G','PG','PG-13','R','NC-17'); END IF; END$$;

ALTER TABLE film ALTER COLUMN rating TYPE rating_t using rating::rating_t;

ALTER TABLE film add CONSTRAINT special_features check (ARRAY['Trailers','Commentaries','Deleted Scenes','Behind the Scenes'] @> string_to_array(special_features,','));

