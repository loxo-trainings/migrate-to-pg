ALTER TABLE actor ALTER COLUMN last_update set default current_timestamp;

ALTER TABLE address ALTER COLUMN last_update set default current_timestamp;

ALTER TABLE category ALTER COLUMN last_update set default current_timestamp;

ALTER TABLE city ALTER COLUMN last_update set default current_timestamp;

ALTER TABLE country ALTER COLUMN last_update set default current_timestamp;

ALTER TABLE customer ALTER COLUMN last_update set default current_timestamp;

ALTER TABLE film ALTER COLUMN last_update set default current_timestamp;

ALTER TABLE film_actor ALTER COLUMN last_update set default current_timestamp;

ALTER TABLE film_category ALTER COLUMN last_update set default current_timestamp;

ALTER TABLE inventory ALTER COLUMN last_update set default current_timestamp;

ALTER TABLE language ALTER COLUMN last_update set default current_timestamp;

ALTER TABLE rental ALTER COLUMN last_update set default current_timestamp;

ALTER TABLE staff ALTER COLUMN last_update set default current_timestamp;

ALTER TABLE store ALTER COLUMN last_update set default current_timestamp;


ALTER TABLE customer ALTER COLUMN active SET  DEFAULT  true ;

ALTER TABLE staff ALTER COLUMN active SET  DEFAULT  true ;

ALTER TABLE customer ALTER COLUMN create_date SET  DEFAULT  ('now'::text)::date;

ALTER TABLE film ALTER COLUMN rental_duration  SET DEFAULT 3 ;
ALTER TABLE film ALTER COLUMN rental_rate  SET DEFAULT 4.99 ;
ALTER TABLE film ALTER COLUMN replacement_cost  SET DEFAULT 19.99;