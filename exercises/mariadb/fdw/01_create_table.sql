
CREATE SCHEMA IF NOT EXISTS sakila;
ALTER SCHEMA sakila OWNER TO ATTENDEE;
GRANT USAGE ON SCHEMA sakila TO attendee;
SET search_path to sakila, public, "$user";
SELECT 'CREATE TABLE sakila.'|| table_name || ' AS SELECT * FROM foreign_sakila.'||table_name
  FROM  information_schema.tables
  WHERE table_schema = 'foreign_sakila' and table_type = 'FOREIGN' \gexec


SELECT 'ALTER TABLE sakila.'|| tablename || ' OWNER TO attendee; '
  FROM  pg_tables
  WHERE schemaname = 'sakila'\gexec


