CREATE EXTENSION mysql_fdw;
CREATE SERVER mariadb FOREIGN DATA WRAPPER mysql_fdw OPTIONS (host 'localhost', port '3306');
GRANT USAGE ON FOREIGN SERVER mariadb TO attendee;
CREATE USER MAPPING FOR postgres SERVER mariadb OPTIONS (username 'attendee',  password 'migrationTraining19');
CREATE USER MAPPING FOR attendee SERVER mariadb OPTIONS (username 'sakila',  password 'migrationTraining19');
CREATE SCHEMA IF NOT EXISTS foreign_sakila;
GRANT USAGE ON SCHEMA foreign_sakila TO attendee;
ALTER SCHEMA foreign_sakila OWNER TO ATTENDEE;
ALTER DATABASE sakila_mariadb_mysql_fdw SET search_path to  sakila,foreign_sakila ,public, "$user";
SET search_path to foreign_sakila, public, "$user";


IMPORT FOREIGN SCHEMA sakila EXCEPT (film, film,actor_info, customer_list, film_list, nicer_but_slower_film_list, sales_by_film_category, sales_by_store, staff_list)
  FROM SERVER mariadb INTO foreign_sakila ;

CREATE FOREIGN TABLE foreign_sakila.film (
film_id int NOT NULL,
title varchar(255) NOT NULL,
rating text,
description text,
release_year int,
language_id smallint NOT NULL,
original_language_id smallint,
rental_duration smallint DEFAULT 3 NOT NULL,
rental_rate numeric(4,2) DEFAULT 4.99 NOT NULL,
length smallint,
replacement_cost numeric(5,2) DEFAULT 19.99 NOT NULL,
special_features text,
last_update timestamptz default CURRENT_TIMESTAMP
) SERVER mariadb OPTIONS (dbname 'sakila', table_name 'film');

SELECT 'ALTER TABLE foreign_sakila.'|| table_name || ' OWNER TO attendee; '
  FROM  information_schema.tables
  WHERE table_schema = 'foreign_sakila' \gexec


GRANT ALL ON ALL TABLES IN SCHEMA foreign_sakila TO attendee;


--- Very helpful to prepare migration
-- The idea is to query the mariadb sys catalog
CREATE SCHEMA IF NOT EXISTS foreign_information_schema;
GRANT USAGE ON SCHEMA foreign_information_schema TO attendee;
ALTER SCHEMA foreign_information_schema OWNER TO ATTENDEE;
ALTER DATABASE sakila_mariadb_mysql_fdw SET search_path to  sakila, foreign_information_schema ,public, "$user";

IMPORT FOREIGN SCHEMA information_schema  FROM SERVER mariadb INTO foreign_information_schema ;
