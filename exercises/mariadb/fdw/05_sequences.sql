CREATE SEQUENCE customer_customer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1 ;

CREATE SEQUENCE actor_actor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE category_category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE film_film_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE address_address_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE city_city_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE country_country_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE inventory_inventory_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE language_language_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE payment_payment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE rental_rental_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE staff_staff_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE store_store_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

SELECT 'ALTER TABLE sakila.'|| sequencename || ' OWNER TO attendee; '
  FROM  pg_sequences
  WHERE schemaname = 'sakila'\gexec

ALTER TABLE actor ALTER COLUMN actor_id SET DEFAULT nextval('actor_actor_id_seq');
ALTER TABLE address ALTER COLUMN address_id SET DEFAULT nextval('address_address_id_seq');
ALTER TABLE category ALTER COLUMN category_id SET DEFAULT nextval('category_category_id_seq');
ALTER TABLE city ALTER COLUMN city_id SET DEFAULT nextval('city_city_id_seq');
ALTER TABLE country ALTER COLUMN country_id SET DEFAULT nextval('country_country_id_seq');
ALTER TABLE customer ALTER COLUMN customer_id SET DEFAULT nextval('customer_customer_id_seq');
ALTER TABLE film ALTER COLUMN film_id SET DEFAULT nextval('film_film_id_seq');
ALTER TABLE inventory ALTER COLUMN inventory_id SET DEFAULT nextval('inventory_inventory_id_seq');
ALTER TABLE language ALTER COLUMN language_id SET DEFAULT nextval('language_language_id_seq');
ALTER TABLE payment ALTER COLUMN payment_id SET DEFAULT nextval('payment_payment_id_seq');
ALTER TABLE rental ALTER COLUMN rental_id SET DEFAULT nextval('rental_rental_id_seq');
ALTER TABLE staff ALTER COLUMN staff_id SET DEFAULT nextval('staff_staff_id_seq');
ALTER TABLE store ALTER COLUMN store_id SET DEFAULT nextval('store_store_id_seq');


ALTER SEQUENCE  actor_actor_id_seq OWNED BY actor.actor_id;
ALTER SEQUENCE  address_address_id_seq OWNED BY address.address_id;
ALTER SEQUENCE  category_category_id_seq OWNED BY category.category_id;
ALTER SEQUENCE  city_city_id_seq OWNED BY city.city_id;
ALTER SEQUENCE  country_country_id_seq OWNED BY country.country_id;
ALTER SEQUENCE  customer_customer_id_seq OWNED BY customer.customer_id;
ALTER SEQUENCE  film_film_id_seq OWNED BY film.film_id;
ALTER SEQUENCE  inventory_inventory_id_seq OWNED BY inventory.inventory_id;
ALTER SEQUENCE  language_language_id_seq OWNED BY language.language_id;
ALTER SEQUENCE  payment_payment_id_seq OWNED BY payment.payment_id;
ALTER SEQUENCE  rental_rental_id_seq OWNED BY rental.rental_id;
ALTER SEQUENCE  staff_staff_id_seq OWNED BY staff.staff_id;
ALTER SEQUENCE  store_store_id_seq OWNED BY store.store_id;



SELECT 'SELECT setval('''||seq.sequence_name||''', max("'||a.attname||'")) FROM "'||d.refobjid::regclass||'";'
FROM   pg_depend    d
JOIN   pg_attribute a ON a.attrelid = d.refobjid
  AND a.attnum   = d.refobjsubid
JOIN  pg_class as pg_class_seq on pg_class_seq.relname::regclass = d.objid and pg_class_seq.relkind = 'S'
JOIN  information_schema.sequences seq on seq.sequence_schema = 'sakila' and pg_class_seq.relname = seq.sequence_name
WHERE d.refobjsubid > 0
AND    d.classid = 'pg_class'::regclass \gexec