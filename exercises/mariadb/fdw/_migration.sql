BEGIN;

\set ON_ERROR_STOP ON

\i 00_create_fdw.sql
\i 01_create_table.sql
\i 02_alter_colum_type.sql
\i 03_null_constraints.sql
\i 04_index_constraints.sql
\i 05_sequences.sql
\i 06_default_values.sql
\i 07_triggers.sql
\i 08_aggregate_functions.sql

COMMIT;