WITH mariadb_constraints AS (
     SELECT
          TC. "CONSTRAINT_SCHEMA" AS constraint_schema,
          TC. "CONSTRAINT_NAME" AS constraint_name,
          TC. "TABLE_NAME" AS table_name ,
          TC. "CONSTRAINT_TYPE" AS constraint_type,
          -- KCU. "TABLE_NAME",
          KCU. "COLUMN_NAME" AS column_name,
          KCU. "REFERENCED_TABLE_NAME" AS referenced_table_name  ,
          KCU. "REFERENCED_COLUMN_NAME" AS referenced_column_name
          -- ,RC. "UPDATE_RULE",
          -- RC. "DELETE_RULE"
     FROM
          foreign_information_schema. "TABLE_CONSTRAINTS" TC
          INNER JOIN foreign_information_schema. "KEY_COLUMN_USAGE" KCU ON TC. "CONSTRAINT_CATALOG" = KCU. "CONSTRAINT_CATALOG"
               AND TC. "CONSTRAINT_SCHEMA" = KCU. "CONSTRAINT_SCHEMA"
               AND TC. "CONSTRAINT_NAME" = KCU. "CONSTRAINT_NAME"
               AND TC. "TABLE_NAME" = KCU. "TABLE_NAME"
     LEFT JOIN foreign_information_schema. "REFERENTIAL_CONSTRAINTS" RC ON TC. "CONSTRAINT_CATALOG" = RC. "CONSTRAINT_CATALOG"
          AND TC. "CONSTRAINT_SCHEMA" = RC. "CONSTRAINT_SCHEMA"
          AND TC. "CONSTRAINT_NAME" = RC. "CONSTRAINT_NAME"
          AND TC. "TABLE_NAME" = RC. "TABLE_NAME"
WHERE
     TC. "CONSTRAINT_SCHEMA" = 'sakila'

),
pg_constraints AS (
            SELECT
               tc.constraint_schema,
               tc.constraint_name,
               tc.table_name,
               tc.constraint_type,
               kcu.table_name as table_columnusage,
               kcu.column_name as columnusage,
               rel_tco.table_name as referenced_table,
               kcu.column_name as referenced_column
               -- ,rc.update_rule,
               -- rc.delete_rule
          FROM
               information_schema.table_constraints tc
          INNER JOIN information_schema.key_column_usage kcu ON tc.constraint_catalog = kcu.constraint_catalog
               AND tc.constraint_schema = kcu.constraint_schema
               AND tc.constraint_name = kcu.constraint_name
               AND tc.table_name = kcu.table_name
          LEFT JOIN information_schema.referential_constraints rc ON tc.constraint_catalog = rc.constraint_catalog
          AND tc.constraint_schema = rc.constraint_schema
          AND tc.constraint_name = rc.constraint_name
          LEFT  join information_schema.table_constraints rel_tco
          ON rc.unique_constraint_schema = rel_tco.constraint_schema
          and rc.unique_constraint_name = rel_tco.constraint_name
UNION
     SELECT
     nspname AS constraint_schema,
     ind_rel.relname AS constraint_name,
     t_rel.relname AS table_name,
     'UNIQUE' AS constraint_type,
     null table_columnusage,
     a.attname AS columnusage,
     null AS referenced_table,
     null AS referenced_column
     FROM pg_index ind
        JOIN pg_class ind_rel ON ind.indexrelid = ind_rel.oid
        JOIN pg_class t_rel ON ind.indrelid = t_rel.oid
        JOIN pg_attribute a on   a.attrelid = t_rel.oid
          and a.attnum = ANY(ind.indkey)
          join pg_namespace ON ind_rel.relnamespace=pg_namespace.oid WHERE nspname = 'sakila' and ind.indisunique = true and ind.indisprimary = false

    )
     SELECT mr.*
     FROM mariadb_constraints mr
     LEFT JOIN pg_constraints pg
          ON mr.table_name =  pg.table_name  AND  mr.constraint_type = pg.constraint_type
     and mr.column_name = pg.columnusage
     WHERE pg.table_name is null
     order by mr.table_name;


