CREATE EXTENSION mysql_fdw;
CREATE SERVER mariadb FOREIGN DATA WRAPPER mysql_fdw OPTIONS (host 'localhost', port '3306');
GRANT USAGE ON FOREIGN SERVER mariadb TO attendee;
CREATE USER MAPPING FOR postgres SERVER mariadb OPTIONS (username 'attendee',  password 'migrationTraining19');
CREATE USER MAPPING FOR attendee SERVER mariadb OPTIONS (username 'attendee',  password 'migrationTraining19');
CREATE SCHEMA IF NOT EXISTS foreign_information_schema;
GRANT USAGE ON SCHEMA foreign_information_schema TO attendee;
ALTER SCHEMA foreign_information_schema OWNER TO ATTENDEE;
ALTER DATABASE sakila_mariadb_mysql_fdw SET search_path to  sakila, foreign_information_schema ,public, "$user";

IMPORT FOREIGN SCHEMA information_schema  FROM SERVER mariadb INTO foreign_information_schema ;
