with mariadb_fk as (
     select
     (fks."CONSTRAINT_SCHEMA" ||'.'|| fks."TABLE_NAME") as foreign_table,
     '->' as rel,
     (fks."UNIQUE_CONSTRAINT_SCHEMA"|| '.'|| fks."REFERENCED_TABLE_NAME") as primary_table,
     fks."CONSTRAINT_NAME",
     string_agg(kcu."COLUMN_NAME", ', ') fk_columns
from foreign_information_schema."REFERENTIAL_CONSTRAINTS" fks
join foreign_information_schema."KEY_COLUMN_USAGE" kcu
     on fks."CONSTRAINT_SCHEMA" = kcu."TABLE_SCHEMA"
     and fks."TABLE_NAME" = kcu."TABLE_NAME"
     and fks."CONSTRAINT_NAME" = kcu."CONSTRAINT_NAME"
group by fks."CONSTRAINT_SCHEMA",
         fks."TABLE_NAME",
         fks."UNIQUE_CONSTRAINT_SCHEMA",
         fks."REFERENCED_TABLE_NAME",
         fks."CONSTRAINT_NAME"
order by fks."CONSTRAINT_SCHEMA",
         fks."TABLE_NAME"
)
,pg_fk as (
select kcu.table_schema || '.' ||kcu.table_name as pg_foreign_table,
       '-->' as rel,
       rel_tco.table_schema || '.' || rel_tco.table_name as pg_primary_table,
       string_agg(kcu.column_name, ', ') as pg_fk_columns,
       kcu.constraint_name as pg_constraint_name
from information_schema.table_constraints tco
join information_schema.key_column_usage kcu
          on tco.constraint_schema = kcu.constraint_schema
          and tco.constraint_name = kcu.constraint_name
join information_schema.referential_constraints rco
          on tco.constraint_schema = rco.constraint_schema
          and tco.constraint_name = rco.constraint_name
join information_schema.table_constraints rel_tco
          on rco.unique_constraint_schema = rel_tco.constraint_schema
          and rco.unique_constraint_name = rel_tco.constraint_name
where
tco.constraint_type = 'FOREIGN KEY'
and
kcu.table_schema = 'sakila'
group by kcu.table_schema,
         kcu.table_name,
         rel_tco.table_name,
         rel_tco.table_schema,
         kcu.constraint_name
order by kcu.table_schema,
         kcu.table_name
)

select * from mariadb_fk
     left join pg_fk on
     foreign_table = pg_foreign_table
     and primary_table = pg_primary_table
     and fk_columns = pg_fk_columns
where pg_fk.pg_primary_table is null;



