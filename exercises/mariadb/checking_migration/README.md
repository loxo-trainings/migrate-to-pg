This queries help you to compare the source schema and the destination.

To use it you have to create fdw between PostgreSQL and Mariadb.
The 00_mariadb_fdw_information_schema.sql help you to do this (postgre
connection is required).

01_constraints_list_mariadb_pg.sql shows you which contrains haven't been
migrated.

02_fk_list_mariadb_pg.sql shows you which fk haven't been migrated.

03_indexes_list_mariadb_pg.sql shows you which index haven't been migrated.

You can do the same for the functions, procedures, tables, sequences

