-- WITH indexes_count_mariadb AS (
--  SELECT "TABLE_NAME" AS mariadb_table_name,
--      COUNT(DISTINCT ("TABLE_NAME", "INDEX_NAME")) mariadb_indexes_count
--      FROM foreign_information_schema."STATISTICS"
--      WHERE "TABLE_SCHEMA" = 'sakila'
--      GROUP BY "TABLE_NAME" ORDER BY "TABLE_NAME"
-- ),
-- indexes_count_pg as(
-- SELECT
--     tablename AS pg_table_name,
--     COUNT((tablename, indexname)) AS pg_indexes_count
-- FROM
--     pg_indexes
-- WHERE
--     schemaname = 'sakila'
-- group BY
--     tablename
-- )
-- SELECT *
--  FROM
--      indexes_count_mariadb
-- LEFT JOIN
--      indexes_count_pg on mariadb_table_name = pg_table_name
-- where indexes_count_mariadb.mariadb_indexes_count <> indexes_count_pg.pg_indexes_count;




--  SELECT 'CREATE INDEX '|| "INDEX_NAME" || ' ON '||"TABLE_NAME"|| ' USING btree ('||"COLUMN_NAME"||');'
--      FROM foreign_information_schema."STATISTICS"
--      WHERE "TABLE_SCHEMA" = 'sakila' and "INDEX_NAME" <> 'PRIMARY';

WITH list_mariadb_indexes as(
SELECT
     "TABLE_NAME" AS table_name,
     string_agg ("COLUMN_NAME",',') AS columns_name,
     "INDEX_NAME" as index_name
     FROM foreign_information_schema."STATISTICS"
     WHERE "TABLE_SCHEMA" = 'sakila' and "INDEX_NAME" <> 'PRIMARY'
     group by "TABLE_NAME","INDEX_NAME"
),
list_pg_indexes as (
    SELECT
        t_rel.relname AS table_name,
        --string_agg(a.attname, ',') AS column_name,
        string_agg(a.attname,',') as columns_name,
        ind_rel.relname AS index_name
    FROM pg_index ind
        JOIN pg_class ind_rel ON ind.indexrelid = ind_rel.oid
        JOIN pg_class t_rel ON ind.indrelid = t_rel.oid
        JOIN pg_attribute a on   a.attrelid = t_rel.oid
        and a.attnum = ANY(ind.indkey)
        join pg_namespace ON ind_rel.relnamespace=pg_namespace.oid
        WHERE nspname = 'sakila'
        group by table_name, index_name
)
--select * from list_pg_indexes;
 select mr.*, '-->', pg.* from list_mariadb_indexes mr
 left join list_pg_indexes pg using(table_name,columns_name)
 where pg.table_name is null
 order by pg.table_name;

