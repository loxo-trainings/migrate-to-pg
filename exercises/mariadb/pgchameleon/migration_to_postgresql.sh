#!/bin/bash

export PYTHONWARNINGS="ignore"
MASTERPWD="migrationTraining19"

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

function format_echo ()
{
    echo -e  "*************************
      $*
    *************************"
}

function pause()
{
    format_echo $*
    read -p "press Enter ..."
}


function replication_status () {
  echo -e " Let's see the replication status
  psql -U attendee -d sakila_mariadb_pgchameleon -c 'select * from sch_chameleon.t_sources;
  "
  read -p "press Enter ..."
  psql -U attendee -d sakila_mariadb_pgchameleon -x -c "select * from sch_chameleon.t_sources;"
}


pause  "source $HOME/venv/bin/activate"
source $HOME/venv/bin/activate

## I present to you on method to migrate from mariadb with pgchameleon
## After the installation and configuration, you have can find in the file pgchameleon_installation.sh and pgchameleon_dependencies.sh
##
## We start the migration
## First pgChamelon works in an Virtual python environnement. So go to the installation directory in our case it's the home directory...
##
## the first command create_schema_replica_schema creates replica schema into the destination database (call here sakila_mariadb_pgchameleon), this schema contains the replication catalog...
## the second command  add_source insert into the replication catalog the source information ....
# source $DIR"/00_init_pgchameleon.sh"



format_echo "chameleon create_replica_schema --config mariadb_sakila --debug"
chameleon create_replica_schema --config mariadb_sakila --debug

format_echo "chameleon add_source --config mariadb_sakila  --debug --source mariadb_sakila"
chameleon add_source --config mariadb_sakila  --debug --source mariadb_sakila

pause " psql -U attendee -d sakila_mariadb_pgchameleon -c '\d'"
psql -U attendee -d sakila_mariadb_pgchameleon -c '\d'


replication_status

## Before init replication we need to prepare some stuffs ..
## If you remember in the configuratioon we can override the default convertion....
## In my case i decided to keep the type year, but it doesn't exit in PostgreSQL
## So I created a Domain year...

## Another set type will be managed later with an additional check on a column.
## Some types has been converted..
# but we can do better...
## Show default.yml...
## You can prepare specif type

pause " Now let's create the specific types found in  mariadb "
psql -U attendee -e -d sakila_mariadb_pgchameleon -f $DIR"/01_create_type.sql"

pause " Ready to synchronise the schema "
chameleon init_replica --config mariadb_sakila --debug  --source mariadb_sakila
sleep 2

##
##  Check quickly what we have to migrate manually...
##
##

replication_status

pause " What does the destination looks like ? "
## pgChameleon create all table and sequence,
## Except specific mariadb type, the conversion is correct...
## You still have to do a check .
psql -U attendee -d sakila_mariadb_pgchameleon -c '\d'


pause " Functions, triggers ... "


pause "Have the functions been migrated ?"


psql -U attendee -d sakila_mariadb_pgchameleon -c '\df sakila.*'


pause "Have the triggers been migrated ?"


psql -U attendee -d sakila_mariadb_pgchameleon -c "select tgname,relname,nspname from pg_trigger join pg_class on tgrelid = pg_class.oid JOIN pg_namespace on relnamespace=pg_namespace.oid where nspname = 'sakila';";


pause " So create all functions, triggers "


psql -U attendee -d sakila_mariadb_pgchameleon -f $DIR"/03_aggregate_functions.sql"
psql -U attendee -d sakila_mariadb_pgchameleon -f $DIR"/04_triggers.sql"



pause "Have the views been migrated ?"


psql -U attendee -d sakila_mariadb_pgchameleon -c "select * from pg_views where schemaname = 'sakila';"


pause " Let's create the views "
# echo SHOW FULL TABLES IN sakila WHERE TABLE_TYPE LIKE 'VIEW' | mysql  -B -u sakila --password=${MASTERPWD}
# select * from pg_views where schemaname = 'sakila';
psql -U attendee -d sakila_mariadb_pgchameleon -f $DIR"/05_view.sql"



pause " Indexes ? "
psql -U attendee -d sakila_mariadb_pgchameleon -c "SELECT count(*) as postgresql_indexes_count FROM  pg_indexes WHERE schemaname = 'sakila';"


pause "Look at the source\n SELECT count(DISTINCT table_name, index_name) as mariadb_sakila_indexes_count FROM INFORMATION_SCHEMA.STATISTICS WHERE TABLE_SCHEMA = 'sakila';"

echo "SELECT count(DISTINCT table_name, index_name) as mariadb_sakila_indexes_count FROM INFORMATION_SCHEMA.STATISTICS WHERE TABLE_SCHEMA = 'sakila';"| mysql  -B -u sakila --password=${MASTERPWD}



pause 'The missing index come from the table film_text. '

echo "SELECT TABLE_SCHEMA, TABLE_NAME, INDEX_NAME, INDEX_TYPE FROM INFORMATION_SCHEMA.STATISTICS WHERE TABLE_SCHEMA = 'sakila' and table_name ='film_text';"| mysql  -B -u sakila --password=${MASTERPWD}

pause ' Add missing indexes '

psql -U attendee -d  sakila_mariadb_pgchameleon -f $DIR"/06_indexes.sql" -e

##
## This tools replicate data
## If you database is quite big, it's interesting to use the replication to limit the downtime.
## So Let's go, start the replica...
##

pause ' Start replica '
chameleon start_replica --config mariadb_sakila --source mariadb_sakila


replication_status
# We simulate some queries on the database source ....
pause " Now insert data into the source to test the replication
  INSERT INTO actor (last_name, first_name) values ('Allen','Rick');
  INSERT INTO actor (last_name, first_name) values ('Davis','John');
"

echo -e "INSERT INTO actor (last_name, first_name) values ('Allen','Rick');\n INSERT INTO actor (last_name, first_name) values ('Davis','John');" | mysql  -B -u sakila --password=${MASTERPWD} sakila
echo -e "SELECT * from actor order by actor_id desc limit 10;" | mysql  -B -u sakila --password=${MASTERPWD} sakila


pause " The data has been replicated  ? "
psql -U attendee -d sakila_mariadb_pgchameleon -e -c "select * from actor order by actor_id desc limit 10";


pause " All data are sychronize.. Stop replica  "
chameleon stop_replica --config mariadb_sakila --source mariadb_sakila
replication_status

## When pgChamelon detach the source, the tool validate all fk.
pause ' Detach from the source  '
chameleon detach_replica --config mariadb_sakila --debug  --source mariadb_sakila
replication_status

pause " Checking the constraints list "

echo "Postgresql side "
psql -U attendee -d sakila_mariadb_pgchameleon -f $DIR"/07_constraints.sql"


echo "Mariadb side "
echo -e "SELECT count(*) mariadb_count_constraints FROM information_schema.table_constraints WHERE table_schema = 'sakila'" | mysql  -B -u sakila --password=${MASTERPWD} sakila

# one more because we added special_features checks..


pause " The default values are not migrated "

psql -U attendee -d sakila_mariadb_pgchameleon -c '\d'

psql -U attendee -d sakila_mariadb_pgchameleon -f $DIR"/08_default_values.sql"

pause " Enable all triggers "

psql -U attendee -d sakila_mariadb_pgchameleon -c "select from fn_active_trigger_schema(true, 'sakila');" -e

pause  " Migration is over ! "