-- only btree
-- MySQL

-- MariaDB [sakila]>
--   SELECT count(DISTINCT table_name, index_name) FROM INFORMATION_SCHEMA.STATISTICS WHERE TABLE_SCHEMA = 'sakila';
--  41

-- sakila_mariadb_pgchameleon=#
-- SELECT
--     count(*)
-- FROM
--     pg_indexes
-- WHERE
--     schemaname = 'sakila';
--  count
-- -------
--     40
-- (1 row)

-- -- So  1 index misses,

-- MariaDB [sakila]>
-- SELECT DISTINCT table_name, count(distinct table_name, index_name, column_name) FROM INFORMATION_SCHEMA.STATISTICS WHERE TABLE_SCHEMA = 'sakila' group by table_name;

-- table_name    | count(distinct table_name, index_name) |
-- +---------------+----------------------------------------+
-- | actor         |                                      2 |
-- | address       |                                      2 |
-- | category      |                                      1 |
-- | city          |                                      2 |
-- | country       |                                      1 |
-- | customer      |                                      4 |
-- | film          |                                      4 |
-- | film_actor    |                                      2 |
-- | film_category |                                      2 |
-- | film_text     |                                      2 |
-- | inventory     |                                      3 |
-- | language      |                                      1 |
-- | payment       |                                      4 |
-- | rental        |                                      5 |
-- | staff         |                                      3 |
-- | store         |                                      3 |
-- +---------------+----------------------------------------+




-- sakila_mariadb_pgchameleon=#
-- SELECT
--     tablename,
--     count(*)
-- FROM
--     pg_indexes
-- WHERE
--     schemaname = 'sakila'
-- group BY
--     tablename;



-- tablename   | count
-- ---------------+-------
--  actor         |     2
--  address       |     2
--  category      |     1
--  city          |     2
--  country       |     1
--  customer      |     4
--  film          |     4
--  film_actor    |     2
--  film_category |     2
--  film_text     |     1
--  inventory     |     3
--  language      |     1
--  payment       |     4
--  rental        |     5
--  staff         |     3
--  store         |     3
-- (16 rows)


-- -- missing in film_text
-- -- Let's see why
-- SELECT * FROM INFORMATION_SCHEMA.STATISTICS WHERE TABLE_SCHEMA = 'sakila' and table_name ='film_text';

-- +---------------+--------------+------------+------------+--------------+-----------------------+--------------+-------------+-----------+-------------+----------+--------+----------+------------+---------+---------------+
-- | TABLE_CATALOG | TABLE_SCHEMA | TABLE_NAME | NON_UNIQUE | INDEX_SCHEMA | INDEX_NAME            | SEQ_IN_INDEX | COLUMN_NAME | COLLATION | CARDINALITY | SUB_PART | PACKED | NULLABLE | INDEX_TYPE | COMMENT | INDEX_COMMENT |
-- +---------------+--------------+------------+------------+--------------+-----------------------+--------------+-------------+-----------+-------------+----------+--------+----------+------------+---------+---------------+
-- | def           | sakila       | film_text  |          0 | sakila       | PRIMARY               |            1 | film_id     | A         |        1000 |     NULL | NULL   |          | BTREE      |         |               |
-- | def           | sakila       | film_text  |          1 | sakila       | idx_title_description |            1 | title       | NULL      |        NULL |     NULL | NULL   |          | FULLTEXT   |         |               |
-- | def           | sakila       | film_text  |          1 | sakila       | idx_title_description |            2 | description | NULL      |        NULL |     NULL | NULL   | YES      | FULLTEXT   |         |               |
-- +---------------+--------------+------------+------------+--------------+-----------------------+--------------+-------------+-----------+-------------+----------+--------+----------+------------+---------+---------------+
-- 3 rows in set (0.00 sec)

-- -- mmmmh ... Interesting thing FullText


-- -- If you need full text search, one solution is to create a new column
-- -- with ts_vector type and migrate data like this.
-- -- Of course you have to create a trigger to update this column
ALTER TABLE film_text add column fulltext tsvector;
CREATE TRIGGER film_fulltext_trigger BEFORE INSERT OR UPDATE ON film_text FOR EACH ROW EXECUTE PROCEDURE tsvector_update_trigger('fulltext', 'pg_catalog.english', 'title', 'description');
UPDATE film_text set fulltext = setweight(to_tsvector('pg_catalog.english',coalesce(title,'')), 'A') ||setweight(to_tsvector('pg_catalog.english',coalesce(description,'')), 'D');
CREATE INDEX film_fulltext_idx ON film_text USING gist (fulltext);
-- In this case a code modification is necessary to fully use the fulltext
-- search...
-- I choose gist because it is lighter than gin and less costly on update command,
-- of course, the performance  is not the same.
-- The best thing would be to add this column in the table film... But it's not
-- the exercice today..





-- SELECT con.conname as constname, rel.relname as tablename  FROM pg_catalog.pg_constraint con
--             INNER JOIN pg_catalog.pg_class rel
--                        ON rel.oid = con.conrelid
--             INNER JOIN pg_catalog.pg_namespace nsp
--                        ON nsp.oid = connamespace
--        WHERE nsp.nspname = 'sakila'
-- --              AND rel.relname = '<table name>';
-- UNION
-- SELECT ind_rel.relname as constname, t_rel.relname as tablename
-- from pg_index ind
--         JOIN pg_class ind_rel on ind.indexrelid = ind_rel.oid
--         JOIN pg_class t_rel on ind.indrelid = t_rel.oid
--         --JOIN pg_indexes indx on indx.indexname = pg_class.relname
--         join pg_namespace on ind_rel.relnamespace=pg_namespace.oid where nspname = 'sakila' and ind.indisunique = true and ind.indisprimary = false;


-- SELECT constraint_name,
--        table_schema,
--        table_name
-- FROM information_schema.table_constraints
-- WHERE
-- table_schema = 'sakila'
-- AND constraint_type <> 'UNIQUE'
-- ORDER BY table_schema,
--          constraint_name;


-- select count(*) from pg_index ind join pg_class on indexrelid = pg_class.oid join pg_namespace on relnamespace=pg_namespace.oid where nspname = 'sakila' and ind.indisunique = true and ind.indisprimary = false;

