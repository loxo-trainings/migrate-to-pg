SELECT count(*) as postgresql_count_constraints FROM (
SELECT con.conname as constname, rel.relname as tablename  FROM pg_catalog.pg_constraint con
        INNER JOIN pg_catalog.pg_class rel
                ON rel.oid = con.conrelid
        INNER JOIN pg_catalog.pg_namespace nsp
                ON nsp.oid = connamespace
WHERE nsp.nspname = 'sakila'
UNION
SELECT ind_rel.relname as constname, t_rel.relname as tablename
FROM pg_index ind
        JOIN pg_class ind_rel ON ind.indexrelid = ind_rel.oid
        JOIN pg_class t_rel ON ind.indrelid = t_rel.oid
        join pg_namespace ON ind_rel.relnamespace=pg_namespace.oid
WHERE nspname = 'sakila' and ind.indisunique = true and ind.indisprimary = false
) as count_constraints;



-- -- Contraint to replace type "set"
-- -- We talked about the set type, here is a way to migrate it.
-- -- pgchamelon convert a set type to a text and we added this contraint.
ALTER TABLE film add CONSTRAINT special_features check (ARRAY['Trailers','Commentaries','Deleted Scenes','Behind the Scenes'] @> string_to_array(special_features,','));


