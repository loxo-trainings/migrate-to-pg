#!/usr/bin/env bash

echo "****************************************"
echo "************** PostgreSQL **************"
echo "****************************************"
echo "************* V ${PGVERSION} ***********"
yum install -y https://download.postgresql.org/pub/repos/yum/reporpms/EL-7-x86_64/pgdg-redhat-repo-latest.noarch.rpm
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-PGDG
if [ ${PGVERSION} -eq "12 " ]; then 
    yum-config-manager --enable pgdg12-updates-testing
fi
yum install -y postgresql${PGVERSION} postgresql${PGVERSION}-server postgresql${PGVERSION}-llvmjit postgresql${PGVERSION}-contrib postgresql${PGVERSION}-libs
# FDW + tools
yum install -y pgloader 
yum install -y tds_fdw${PGVERSION} mysql_fdw_${PGVERSION}

# PostgreSQL Cluster initialize
/usr/pgsql-${PGVERSION}/bin/postgresql-${PGVERSION}-setup initdb
systemctl enable postgresql-${PGVERSION}
systemctl start postgresql-${PGVERSION}

# Create a user for attendee
# Hint : try to create database with attendee as owner.
sudo -iu postgres createuser -d -r attendee
sudo -iu postgres MASTERPWD=${MASTERPWD} PGVERSION=${PGVERSION} bash /vagrant/provision/sql/postgresql_init.sh