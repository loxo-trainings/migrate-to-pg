python3.6 -Wignore -m venv venv
source venv/bin/activate
pip install pip --upgrade
pip install pg_chameleon
PYTHONWARNINGS=ignore::yaml.YAMLLoadWarning
chameleon set_configuration_files

sed -e 's/@@MASTERPWD@@/'${MASTERPWD}'/g' /vagrant/provision/config/pgchamelon/default.yml >> /home/attendee/.pg_chameleon/configuration/mariadb_sakila.yml

# chameleon create_replica_schema --debug
# chameleon add_source --config default  --debug --source mysql
# chameleon init_replica --config default --debug --source mysql



