##### Migration start here.

source venv/bin/activate
chameleon create_replica_schema --config default
chameleon add_source --config default  --debug --source mysql


## Some types has been converted..
# release_year YEAR -> integer
## Show default.yml...

## You can prepare the something like that
##
CREATE DOMAIN year AS integer
	CONSTRAINT year_check CHECK (((VALUE >= 1901) AND (VALUE <= 2155)));
ALTER DOMAIN year OWNER TO attendee;
## Just before init_replica
##


chameleon init_replica --config default --source mysql

## Connect to postgresql to control if the data has been imported


# You can start the deamon
# chameleon start_replica --config default  --source mysql
# so in this case all data are updated in live.
# select * from t_sources;
# stop replication
# chameleon stop_replica --config default  --source mysql
# this feature is really interessing if you want to reduce the downtime
# I can't tell you what is the limit of this application with a huge volume of
# transaction ...
# send email to Federico....


# use case
# Stop postgresql CLUSTER
# Look what's happen ....
# systemctl start postgresql-11.service
# insert some data.
# you have to enable the replica manually ...
# chameleon enable_replica --config default  --source mysql
# restart
# chameleon start_replica --config default  --source mysql



# modify ddl... is live too...

#Mariadb
CREATE TABLE my_camel_case_table ( cid MEDIUMINT NOT NULL AUTO_INCREMENT,
   description text,
   a integer check (a > b),
   b integer check (b < a),
   PRIMARY KEY (cid),
   CONSTRAINT a_greater CHECK (a > b)
);

INSERT INTO my_camel_case_table (description,a,b) VALUES  ('description 1', 2, 1);
INSERT INTO my_camel_case_table (description,a,b) VALUES  ('description 2', 4, 3);
ALTER TABLE actor ADD COLUMN perso_info text;


# Postgresql
\d "MyCamelCaseTable"
\d actor

# Mariadb
DROP TABLE MyCamelCaseTable;

# Postgresql
\d

## Manually
# chameleon stop_replica --config default  --source mysql
# chameleon refresh_schema --config default  --source mysql --schema sakila


##  You have to migrate the trigger, function, view...
##


# add this column to show you that in the migration process you have to disable
# trigger, until the switchover is not completed

# You have to c

CREATE OR REPLACE FUNCTION ins_film()  RETURNS TRIGGER AS
$$
BEGIN
    INSERT INTO film_text (film_id, title, description)
        VALUES (new.film_id, new.title, new.description, 'postgresql');
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER trg_ins_film
  BEFORE INSERT OR UPDATE OR DELETE
  ON film
  FOR EACH ROW
  EXECUTE PROCEDURE ins_film();


CREATE OR REPLACE FUNCTION upd_film()  RETURNS TRIGGER AS
$$
BEGIN
    IF (old.title != new.title) or (old.description != new.description)
    THEN
        UPDATE film_text
            SET title=new.title,
                description=new.description,
                last_update_trigger_origin = 'postgresql'
                film_id=new.film_id
        WHERE film_id=old.film_id;
    END IF;
    return NEW;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE TRIGGER trg_upd_film
  BEFORE INSERT OR UPDATE OR DELETE
  ON film
  FOR EACH ROW
  EXECUTE PROCEDURE upd_film();

## Disable trigger ....
CREATE OR REPLACE FUNCTION fn_triggerall(DoEnable boolean) RETURNS integer AS
$BODY$
DECLARE
mytables RECORD;
BEGIN
  FOR mytables IN SELECT relname FROM pg_class WHERE reltriggers > 0 AND NOT relname LIKE 'pg_%'
  LOOP
    IF DoEnable THEN
      EXECUTE 'ALTER TABLE ' || mytables.relname || ' ENABLE TRIGGER ALL';
    ELSE
      EXECUTE 'ALTER TABLE ' || mytables.relname || ' DISABLE TRIGGER ALL';
    END IF;
  END LOOP;
RETURN 1;
$BODY$ LANGUAGE plpgsql;
select from fn_triggerall(false);









chameleon detach_replica --config default  --source mysql



## Exemple of a sequence setting ....


SELECT setval('"MyCamelCaseTable_cID_seq"', max("cID")) FROM "MyCamelCaseTable";
# CREATE automated sequence setting.
INSERT INTO "MyCamelCaseTable" ("Description") VALUES
  ('description 1'),
  ('description 2'),
  ('description 3'),
  ('description 4');

SELECT currval('"MyCamelCaseTable_cID_seq"');


ALTER TABLE film add CONSTRAINT special_features check (special_features in ('Trailers','Commentaries','Deleted Scenes','Behind the Scenes'));


# Get ALl columns ...
MariaDB [sakila]> select col.table_schema, col.table_name, column_name, data_type from information_schema.columns as col join information_schema.tables as tab  on tab.table_name= col.table_name  where col.table_schema = 'sakila' and tab.table_type = 'BASE TABLE' order by col.table_name,ordinal_position;


# Set all sequence...
SELECT 'SELECT setval('''||seq.sequence_name||''', max("'||a.attname||'")) FROM "'||d.refobjid::regclass||'";'
FROM   pg_depend  d
JOIN   pg_attribute a ON a.attrelid = d.refobjid AND a.attnum   = d.refobjsubid
JOIN  pg_class as pg_class_seq on pg_class_seq.relname::regclass = d.objid AND pg_class_seq.relkind = 'S'
JOIN  information_schema.sequences seq on seq.sequence_schema = 'sakila' AND pg_class_seq.relname = seq.sequence_name
WHERE d.refobjsubid > 0
AND    d.classid = 'pg_class'::regclass \gexec


# 2 possibilities :
# 1 let the type text and add contraint with a cast to array and the operator contain
ALTER TABLE film add CONSTRAINT special_features check (ARRAY['Trailers','Commentaries','Deleted Scenes','Behind the Scenes'] @> string_to_array(special_features,','));

# Or convert string to text array and add contraint check ..
ALTER TABLE film add special_features_arr text[];
UPDATE film set special_features_arr = string_to_array(special_features,',');
ALTER TABLE film drop column special_features;
ALTER TABLE film rename special_features_arr to special_features;
ALTER TABLE film add CONSTRAINT special_features check (ARRAY['Trailers','Commentaries','Deleted Scenes','Behind the Scenes'] @> special_features));


