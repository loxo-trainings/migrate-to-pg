# MariaDB install
echo "*************************************"
echo "************** MARIADB **************"
echo "*************************************"
yum install -y mariadb-server mariadb
systemctl enable mariadb
systemctl start mariadb
bash /vagrant/provision/sql/mariadb_sakila_init.sh
bash /vagrant/provision/pgchameleon_dependencies.sh
sudo -iu attendee MASTERPWD=${MASTERPWD} bash /vagrant/provision/pgchameleon_installation.sh

# pgloader for mariadb
bash /vagrant/provision/pgloader_initialisation.sh

# mysql_fdw
sudo -iu postgres MASTERPWD=${MASTERPWD} bash /vagrant/provision/mariadb_sakila_mysql_fdw.sh