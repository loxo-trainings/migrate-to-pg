CREATE EXTENSION tds_fdw;
CREATE SERVER mssql FOREIGN DATA WRAPPER tds_fdw OPTIONS(servername '127.0.0.1', database 'sakila');
GRANT USAGE ON FOREIGN SERVER mssql TO attendee;
CREATE USER MAPPING FOR postgres SERVER mssql OPTIONS (username 'sa',  password '@@MASTERPWD@@');
CREATE USER MAPPING FOR attendee SERVER mssql OPTIONS (username 'sa',  password '@@MASTERPWD@@');
CREATE SCHEMA foreign_sakila;
GRANT USAGE ON SCHEMA foreign_sakila TO attendee;
IMPORT FOREIGN SCHEMA dbo FROM SERVER mssql INTO foreign_sakila OPTIONS (import_default 'false');
GRANT ALL ON ALL TABLES IN SCHEMA foreign_sakila TO attendee;