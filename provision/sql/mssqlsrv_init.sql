-- Initialize SQL Server demo database
USE master;
GO

CREATE DATABASE sakila;
GO

USE sakila;
GO

CREATE LOGIN sakila WITH PASSWORD = '@@MASTERPWD@@' ;
GO

CREATE USER sakila FOR LOGIN sakila WITH DEFAULT_SCHEMA=dbo;
GO

EXEC sp_addrolemember N'db_datareader', N'sakila';
GO

EXEC sp_addrolemember N'db_datawriter', N'sakila';
GO

EXEC sp_addrolemember N'db_owner', N'sakila';
GO

CREATE LOGIN attendee WITH PASSWORD = '@@MASTERPWD@@' ;
GO

CREATE USER attendee FOR LOGIN attendee WITH DEFAULT_SCHEMA=dbo;
GO

EXEC sp_addrolemember N'db_datareader', N'attendee';
GO

EXEC sp_addrolemember N'db_datawriter', N'attendee';
GO

EXEC sp_addrolemember N'db_owner', N'attendee';
GO