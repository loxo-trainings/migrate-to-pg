sed -i "s/#listen_addresses = 'localhost'/listen_addresses = '\*'/g" /var/lib/pgsql/${PGVERSION}/data/postgresql.conf
echo "host     all     attendee            10.0.0.1/24            md5">> /var/lib/pgsql/${PGVERSION}/data/pg_hba.conf
echo "host     all     attendee            10.0.2.1/24            md5">> /var/lib/pgsql/${PGVERSION}/data/pg_hba.conf
sed -i "s/ident/md5/g" /var/lib/pgsql/${PGVERSION}/data/pg_hba.conf
systemctl restart  postgresql-${PGVERSION}.service
sed -e 's/@@MASTERPWD@@/'${MASTERPWD}'/g' /vagrant/provision/sql/postgresql_init.sql|psql