-- Replication user
CREATE USER replica_user IDENTIFIED BY 'replication19';
GRANT ALL PRIVILEGES ON sakila.* TO 'replica_user'@'%' IDENTIFIED BY 'replication19';
GRANT ALL PRIVILEGES ON sakila.* TO 'replica_user'@'localhost' IDENTIFIED BY 'replication19';

GRANT RELOAD ON *.* TO 'replica_user'@'%';
GRANT RELOAD ON *.* TO 'replica_user'@'localhost';

GRANT REPLICATION CLIENT ON *.* TO 'replica_user'@'%';
GRANT REPLICATION CLIENT ON *.* TO 'replica_user'@'localhost';

GRANT REPLICATION SLAVE ON *.* TO 'replica_user'@'%';
GRANT REPLICATION SLAVE ON *.* TO 'replica_user'@'localhost';


FLUSH PRIVILEGES;
