-- Replication user
CREATE USER pgloader_user IDENTIFIED BY 'replication19';
GRANT ALL PRIVILEGES ON sakila.* TO 'pgloader_user'@'%' IDENTIFIED BY 'replication19';
GRANT ALL PRIVILEGES ON sakila.* TO 'pgloader_user'@'localhost' IDENTIFIED BY 'replication19';

GRANT RELOAD ON *.* TO 'pgloader_user'@'%';
GRANT RELOAD ON *.* TO 'pgloader_user'@'localhost';

GRANT REPLICATION CLIENT ON *.* TO 'pgloader_user'@'%';
GRANT REPLICATION CLIENT ON *.* TO 'pgloader_user'@'localhost';

GRANT REPLICATION SLAVE ON *.* TO 'pgloader_user'@'%';
GRANT REPLICATION SLAVE ON *.* TO 'pgloader_user'@'localhost';

-- GRANT SUPER ON *.* TO 'pgloader_user'@'%';
-- GRANT SUPER ON *.* TO 'pgloader_user'@'localhost';
-- GRANT SELECT, PROCESS ON *.*  TO 'pgloader_user'@'%';
-- GRANT SELECT, PROCESS ON *.* TO 'pgloader_user'@'localhost';
FLUSH PRIVILEGES;
