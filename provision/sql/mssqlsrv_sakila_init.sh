SAKILAFILES="sql-server-sakila-schema.sql sql-server-sakila-insert-data.sql"
# CREATE user/schema and granting
sqlcmd -U SA -P "${MASTERPWD}" -i <(sed -e 's/@@MASTERPWD@@/'${MASTERPWD}'/g' /vagrant/provision/sql/mssqlsrv_init.sql)
echo "Processing sql-server-sakila-schema.sql"
sqlcmd -U SA -P "${MASTERPWD}" -i <(curl -sfL "https://raw.githubusercontent.com/jOOQ/jOOQ/master/jOOQ-examples/Sakila/sql-server-sakila-db/sql-server-sakila-schema.sql") 
sqlcmd -U SA -P "${MASTERPWD}" -d sakila -i <(curl -sfL "https://raw.githubusercontent.com/jOOQ/jOOQ/master/jOOQ-examples/Sakila/sql-server-sakila-db/sql-server-sakila-insert-data.sql") 