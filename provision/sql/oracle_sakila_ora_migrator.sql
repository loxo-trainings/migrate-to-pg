CREATE EXTENSION oracle_fdw;
CREATE EXTENSION ora_migrator;
CREATE EXTENSION orafce;

CREATE SERVER oradb FOREIGN DATA WRAPPER oracle_fdw OPTIONS(dbserver '//localhost:1521/XEPDB1');
GRANT USAGE ON FOREIGN SERVER oradb TO attendee;
CREATE USER MAPPING FOR postgres SERVER oradb OPTIONS (user 'sakila', password '@@MASTERPWD@@');
CREATE USER MAPPING FOR attendee SERVER oradb OPTIONS (user 'sakila', password '@@MASTERPWD@@');

-- SELECT oracle_migrate(server => 'oradb', only_schemas => '{SAKILA}');
-- GRANT USAGE ON SCHEMA sakila TO attendee;
-- GRANT ALL ON ALL TABLES IN SCHEMA sakila TO attendee;

-- migration doe not work this way :
--WARNING:  Error creating view sakila.customer_list
--DETAIL:  function decode(character, integer, unknown, unknown) does not exist: 
--
-- We'll need to dive into migration process

-- ------------------------------------------------------------------------------------------
-- * Call oracle_migrate_prepare to create the Oracle staging schema with the Oracle metadata views and the PostgreSQL staging schema with metadata copied and translated from the Oracle stage.
-- * After this step, you can modify the data in the PostgreSQL stage, from which the PostgreSQL tables are created. This is useful if you want to modify data types, indexes or constraints.
--      Be aware that you cannot rename the schemas.
--      Also, if you want to rename tables, make sure that the new name is used in all tables consistently.
--      The tables tables and functions in the PostgreSQL staging schema have a boolean attribute migrate that should be set to TRUE to include the object in the migration. Since functions will always require editing, the flag is initially set to FALSE for functions.
--      If the Oracle definitions change while you are working, you can refresh the tables in the PostgreSQL stage with oracle_migrate_refresh.
-- * Call oracle_migrate_mkforeign to create the PostgreSQL schemas and sequences and foreign tables.
-- * Call oracle_migrate_tables to replace the foreign tables with real tables and migrate the data from Oracle.
--      Alternatively, you can use oracle_materialize to do this step for Each table individually. This has the advantage that you can migrate several tables in parallel in multiple database sessions, which may speed up the migration process.
-- * Call oracle_migrate_functions to migrate functions.
-- * Call oracle_migrate_triggers to migrate triggers.
-- * Call oracle_migrate_views to migrate views.
-- * Call oracle_migrate_constraints to migrate constraints and indexes from Oracle.
-- * Call oracle_migrate_finish to remove the staging schemas and complete the migration.
-- ------------------------------------------------------------------------------------------

-- if time misses : select oracle_migrate_prepare(server=>'oradb', only_schemas=>'{SAKILA}');
-- look at pgsql_stage.views
-- select * from pgsql_stage.views where schema='sakila' and view_name='customer_list';

-- View customer_list contains decode that could be overcome by orafce package maybe
-- decode addresses a boolean : active (char(1) default 'Y' not null)
-- decode(cu.active, 1,'active','') AS notes, 

-- orafce workaround :
-- CREATE view sakila.customer_list AS SELECT cu.customer_id AS ID,
--        cu.first_name||' '||cu.last_name AS name,
--        a.address AS address,
--        a.postal_code AS zip_code,
--        a.phone AS phone,
--        city.city AS city,
--        country.country AS country,
--        decode(cu.active,'1','active','') AS notes,
--        cu.store_id AS SID
-- FROM sakila.customer cu JOIN sakila.address a ON cu.address_id = a.address_id JOIN sakila.city ON a.city_id = city.city_id
--     JOIN sakila.country ON city.country_id = country.country_id

-- No orafce WORKAROUND
-- SELECT cu.customer_id AS ID,
--        cu.first_name||' '||cu.last_name AS name,
--        a.address AS address,
--        a.postal_code AS zip_code,
--        a.phone AS phone,
--        city.city AS city,
--        country.country AS country,
--        CASE WHEN cu.active='1' THEN 'active' ELSE '' AS notes,
--        cu.store_id AS SID
-- FROM customer cu JOIN address a ON cu.address_id = a.address_id JOIN city ON a.city_id = city.city_id
--     JOIN country ON city.country_id = country.country_id


