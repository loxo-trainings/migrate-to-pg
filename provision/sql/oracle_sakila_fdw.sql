CREATE EXTENSION oracle_fdw;
CREATE SERVER oradb FOREIGN DATA WRAPPER oracle_fdw OPTIONS(dbserver '//localhost:1521/XEPDB1');

GRANT USAGE ON FOREIGN SERVER oradb TO attendee;
CREATE USER MAPPING FOR postgres SERVER oradb OPTIONS (user 'sakila', password '@@MASTERPWD@@');
CREATE USER MAPPING FOR attendee SERVER oradb OPTIONS (user 'sakila', password '@@MASTERPWD@@');
CREATE SCHEMA sakila;
GRANT USAGE ON SCHEMA sakila TO attendee;
IMPORT FOREIGN SCHEMA "SAKILA" FROM SERVER oradb INTO sakila;
GRANT ALL ON ALL TABLES IN SCHEMA sakila TO attendee;