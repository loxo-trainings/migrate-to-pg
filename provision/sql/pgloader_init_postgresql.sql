--
CREATE DATABASE sakila_mariadb_pgloader WITH OWNER attendee;
ALTER DATABASE sakila_mariadb_pgloader set search_path to sakila, public;