SAKILAFILES="oracle-sakila-schema.sql oracle-sakila-schema-pl-sql.sql oracle-sakila-insert-data.sql"
# CREATE user/schema and granting
SAKILA_EXISTS=$(echo "SELECT COUNT(*) FROM DBA_USERS WHERE USERNAME='SAKILA';"|sqlplus -s sys/${MASTERPWD}@XEPDB1 as sysdba | tail -2 | head -1 | sed -e 's/^\s*\([0-9]\)*/\1/g')
echo "SAKILA exists ? ${SAKILA_EXISTS}"
if [ ${SAKILA_EXISTS}  -eq "0" ]; then
    echo "init oracle.sql"
    sed -e 's/@@MASTERPWD@@/'${MASTERPWD}'/g' /vagrant/provision/sql/oracle_init.sql | sqlplus -s sys/${MASTERPWD}@XEPDB1 as sysdba 
    echo "Feed Oracle with sakila"
    for ofile in ${SAKILAFILES}; do 
        curl -sfL "https://raw.githubusercontent.com/jOOQ/jOOQ/master/jOOQ-examples/Sakila/oracle-sakila-db/${ofile}" |sqlplus -s sakila/${MASTERPWD}@XEPDB1 
    done
    # Update stats for better analyze
    echo "EXECUTE DBMS_STATS.GATHER_SCHEMA_STATS('SAKILA',DBMS_STATS.AUTO_SAMPLE_SIZE);" | sqlplus sys/${MASTERPWD}@XEPDB1 as sysdba
else 
    echo "Nothing to provision, already in place"; 
fi