SAKILAFILES="mysql-sakila-schema.sql mysql-sakila-insert-data.sql"
# CREATE user/schema and granting
sed -e 's/@@MASTERPWD@@/'${MASTERPWD}'/g' /vagrant/provision/sql/mariadb_init.sql | mysql -u root mysql
for ofile in ${SAKILAFILES}; do
    curl -sfL "https://raw.githubusercontent.com/jOOQ/jOOQ/master/jOOQ-examples/Sakila/mysql-sakila-db/${ofile}" | mysql  -B -u sakila --password="${MASTERPWD}"  sakila
done

