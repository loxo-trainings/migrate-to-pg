CREATE USER sakila IDENTIFIED BY @@MASTERPWD@@;
CREATE USER attendee IDENTIFIED BY @@MASTERPWD@@;
GRANT connect to sakila;
GRANT resource to sakila;
GRANT create session TO sakila;
GRANT create table TO sakila;
GRANT create view TO sakila;
GRANT connect to attendee;
GRANT resource to attendee;
GRANT create session TO attendee;
GRANT create table TO attendee;
GRANT create view TO attendee;
GRANT SELECT ANY DICTIONARY TO attendee;
GRANT UNLIMITED TABLESPACE TO attendee;
GRANT SELECT ANY TABLE TO attendee;
-- for ora_migrator
GRANT SELECT ANY DICTIONARY TO sakila;
GRANT UNLIMITED TABLESPACE TO sakila;
GRANT SELECT ANY TABLE TO sakila;
