cat /vagrant/provision/sql/pgloader_init_mysql.sql | mysql -u root -B sakila

## Create sakila_mariadb_pgloader database and user.
sudo -iu postgres psql -f /vagrant/provision/sql/pgloader_init_postgresql.sql

## Add new pg_hba rules
sudo -iu postgres MASTERPWD=${MASTERPWD} PGVERSION=${PGVERSION} bash /vagrant/provision/sql/pgloader_init_postgresql.sh

systemctl restart  postgresql-${PGVERSION}.service

## Prepare pgloader configuration...
##sudo -iu attendee sed -e 's/@@MASTERPWD@@/'${MASTERPWD}'/g' /vagrant/provision/config/pgloader/mariadb/maria_to_postgresql.load > /vagrant/exercises/mariadb/pgloader/pgloader_mariadb_to_postgres.load