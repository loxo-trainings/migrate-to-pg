echo "************* MASTER PASSWORD *************"
echo ${MASTERPWD}
echo "*******************************************"
echo "tty -s && mesg -n" >> /root/.profile
useradd -m attendee
usermod -G attendee,wheel attendee
echo "attendee:${MASTERPWD}"|chpasswd
cat << EOF >> /home/attendee/.bashrc
export PAGER="less"
export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8
export LANGUAGE=en_US.UTF-8
EOF
chown attendee:attendee /home/attendee/.bashrc
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7
yum install -y http://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-7 
yum update -y
yum install -y vim tmux
sed -in 's/^PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
systemctl restart sshd
