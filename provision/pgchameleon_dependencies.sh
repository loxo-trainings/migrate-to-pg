yum install -y  python36

cat /vagrant/provision/sql/pgchameleon_init_mysql.sql | mysql -u root -B sakila

# Configure the replica
sed -i '/socket=\/var\/lib\/mysql\/mysql.sock/a log-bin = mysql-bin \nserver_id=1 \nbinlog_format = ROW\n' /etc/my.cnf
systemctl restart mariadb

systemctl status mariadb

## Create replication database and user.
runuser -l postgres -c 'psql -f /vagrant/provision/sql/pgchameleon_init_postgresql.sql'

## Add new pg_hba rules
sudo -iu postgres MASTERPWD=${MASTERPWD} PGVERSION=${PGVERSION} bash /vagrant/provision/sql/pgchameleon_init_postgresql.sh
