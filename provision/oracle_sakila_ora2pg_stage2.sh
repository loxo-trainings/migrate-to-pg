# CBASE and MASTERPWD should be defined as projects
ora2pg --init_project ${CBASE}
echo "++++${CBASE}++++"
sed -i.orig 's/^ORACLE_DSN.*$/ORACLE_DSN dbi:Oracle:host=localhost;SERVICE_NAME=XEPDB1;port=1521/g;
    s/^SCHEMA.*CHANGE_THIS_SCHEMA_NAME.*$/SCHEMA SAKILA/g;
    s/^ORACLE_USER.*$/ORACLE_USER sakila/g;
    s/^ORACLE_PWD.*$/ORACLE_PWD '${MASTERPWD}'/g;
    s/^#PG_DSN.*$/PG_DSN          dbi:Pg:dbname='${CBASE}'\;host=localhost\;port=5432/g;
    s/^#PG_USER.*$/PG_USER attendee/g;
    s/^JOBS.*$/JOBS            2/g;
    s/^ORACLE_COPIES.*$/ORACLE_COPIES    2/g;
    s/^#PG_PWD.*$/PG_PWD '${MASTERPWD}'/g' \
    ${CBASE}/config/ora2pg.conf
echo "---"
if [ ${CBASE} = 'sakila_ora_ora2pg_better' ]; then
    # TODO change here
    sed -i 's/^#.*DATA_TYPE.*$/DATA_TYPE       NUMBER(*\,0):bigint/g;
    s/^#REPLACE_AS_BOOLEAN.*$/REPLACE_AS_BOOLEAN     CUSTOMER:ACTIVE/g;
    s/^#BOOLEAN_VALUES.*$/BOOLEAN_VALUES          yes:no y:n 1:0 true:false enabled:disabled Y:N/g;
    s/^#MODIFY_TYPE.*$/MODIFY_TYPE  PAYMENT:amount:numeric(5\\,2)/g' ${CBASE}/config/ora2pg.conf
    # Add the following sed rule if needed
    #s/^#MODIFY_TYPE.*$/MODIFY_TYPE  customer:active:smallint/g' ${CBASE}/config/ora2pg.conf
fi
cd ${CBASE} 
./export_schema.sh
cd ..