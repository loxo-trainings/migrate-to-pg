#!/bin/bash
echo "*************************************"
echo "************** IBM DB2 **************"
echo "*************************************"
yum install -y wget nfs-utils libaio* ksh compat-libstdc++* libstdc++* numactl.x86_64 pam.i686 yum install libstdc++.i686
cd /tmp
tar xf /vagrant/db2/*.tar.gz
cd /tmp/expc
/usr/sbin/groupadd -g 511 db2fgrp1
/usr/sbin/useradd -p db2fenc1 -g db2fgrp1 -m -d /home/db2fenc1 db2fenc1
cat /vagrant/provision/db2expc.rsp | sed -e 's/@@MASTERPASSWD@@/'${MASTERPWD}'/g' > /vagrant/db2/db2expc.rsp
./db2setup -r /vagrant/db2/db2expc.rsp