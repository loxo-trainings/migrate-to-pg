# Create attendee if needed, normally done in postgresql.sh
# createuser attendee;
CBASE="sakila_mssql_pgloader"
createdb -O attendee ${CBASE}
sed -e 's/@@MASTERPWD@@/'${MASTERPWD}'/g' /vagrant/provision/sql/mssqlsrv_sakila_tds_fdw.sql | psql ${CBASE}