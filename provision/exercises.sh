cp -a /vagrant/exercises/ /home/attendee/
chown -R attendee:attendee /home/attendee/exercises

mkdir /var/lib/pgsql/scripts/
mkdir /var/lib/pgsql/scripts/mariadb/
mkdir /var/lib/pgsql/scripts/mssql/

cp -a /home/attendee/exercises/mariadb/fdw/ /var/lib/pgsql/scripts/mariadb/fdw
cp -a /home/attendee/exercises/mariadb/checking_migration/ /var/lib/pgsql/scripts/checking_migration
cp -a /home/attendee/exercises/mssql/fdw/ /var/lib/pgsql/scripts/mssql/fdw

chown -R postgres:postgres /var/lib/pgsql/scripts/