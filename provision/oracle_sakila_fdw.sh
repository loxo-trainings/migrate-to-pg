# Create attendee if needed, normally done in postgresql.sh
# createuser attendee;
CBASE="sakila_ora_ora_fdw"
createdb -O attendee ${CBASE}
sed -e 's/@@MASTERPWD@@/'${MASTERPWD}'/g' /vagrant/provision/sql/oracle_sakila_fdw.sql | psql ${CBASE}