#! /bin/bash
# Server
echo "**************************************"
echo "************** MSSQLSRV **************"
echo "**************************************"
curl -Lso /etc/yum.repos.d/mssql-server.repo https://packages.microsoft.com/config/rhel/7/mssql-server-preview.repo
yum install -y mssql-server
yum install -y mssql-cli


ACCEPT_EULA='Y' MSSQL_PID='Express' MSSQL_SA_PASSWORD="${MASTERPWD}" /opt/mssql/bin/mssql-conf setup
# Tools
curl -Lso /etc/yum.repos.d/msprod.repo https://packages.microsoft.com/config/rhel/7/prod.repo
ACCEPT_EULA='Y' yum install -y mssql-tools unixODBC-devel
echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~attendee/.bash_profile
echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~attendee/.bashrc
sudo -iu attendee MASTERPWD=${MASTERPWD} bash /vagrant/provision/sql/mssqlsrv_sakila_init.sh

# Install sakila with tds_fdw
sudo -iu postgres MASTERPWD=${MASTERPWD} bash /vagrant/provision/mssqlsrv_sakila_tds_fdw.sh
sudo -iu postgres MASTERPWD=${MASTERPWD} bash /vagrant/provision/mssqlsrv_sakila_pgloader.sh
