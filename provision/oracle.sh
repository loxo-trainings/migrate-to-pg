#!/bin/bash
echo "************************************"
echo "************** Oracle **************"
echo "************************************"
BUILD="${HOME}/build/"
# Quick and dirty (ugly) check for Oracle installation...
if [ ! -e /opt/oracle/product/18c/dbhomeXE/bin/sqlplus ]; then 
yum install -y bc bind-utils compat-libcap1 glibc-devel net-tools psmisc smartmontools sysstat unzip xorg-x11-utils xorg-x11-xauth
yum install -y /vagrant/oracle/*preins*rpm
yum install -y /vagrant/oracle/*xe*rpm
# To configure Oracle Database XE, optionally modify the parameters in '/etc/sysconfig/oracle-xe-18c.conf' and then execute '/etc/init.d/oracle-xe-18c configure' as root.
ORACLE_HOME="/opt/oracle/product/18c/dbhomeXE"
sed -i 's/^LISTENER_PORT=.*/LISTENER_PORT=1521/g' /etc/sysconfig/oracle-xe-18c.conf
(echo "${MASTERPWD}";echo "${MASTERPWD}")|/etc/init.d/oracle-xe-18c configure
# Listener
echo "# listener.ora Network Configuration File:

SID_LIST_LISTENER =
(SID_LIST =
    (SID_DESC =
    (SID_NAME = PLSExtProc)
    (ORACLE_HOME = $ORACLE_HOME)
    (PROGRAM = extproc)
    )
)

LISTENER =
(DESCRIPTION_LIST =
    (DESCRIPTION =
    (ADDRESS = (PROTOCOL = IPC)(KEY = EXTPROC_FOR_XE))
    (ADDRESS = (PROTOCOL = TCP)(HOST = 0.0.0.0)(PORT = 1521))
    )
)

DEFAULT_SERVICE_LISTENER = (XE)" > $ORACLE_HOME/network/admin/listener.ora
# TNS Names.ora
echo "# tnsnames.ora Network Configuration File:

XE =
  (DESCRIPTION =
    (ADDRESS = (PROTOCOL = TCP)(HOST = 0.0.0.0)(PORT = 1521))
    (CONNECT_DATA =
      (SERVER = DEDICATED)
      (SERVICE_NAME = XE)
    )
  )

LISTENER_XE =
  (ADDRESS = (PROTOCOL = TCP)(HOST = 0.0.0.0)(PORT = 1521))

XEPDB1 =
  (DESCRIPTION =
    (ADDRESS = (PROTOCOL = TCP)(HOST = 0.0.0.0)(PORT = 1521))
    (CONNECT_DATA =
      (SERVER = DEDICATED)
      (SERVICE_NAME = XEPDB1)
    )
  )

EXTPROC_CONNECTION_DATA =
  (DESCRIPTION =
     (ADDRESS_LIST =
       (ADDRESS = (PROTOCOL = IPC)(KEY = EXTPROC_FOR_XE))
     )
     (CONNECT_DATA =
       (SID = PLSExtProc)
       (PRESENTATION = RO)
     )
  )
" > $ORACLE_HOME/network/admin/tnsnames.ora

# maybe messy or not. It's mandatory to resolve tnsnames
#--- Well
chmod o+r $ORACLE_HOME/network/admin/tnsnames.ora  $ORACLE_HOME/network/admin/listener.ora


echo "ORACLE_HOME=\"${ORACLE_HOME}\"
ORACLE_SID=\"XE\"
export ORACLE_HOME
export ORACLE_SID
export PATH=\$PATH:${ORACLE_HOME}/bin">> /home/attendee/.bash_profile

echo "ORACLE_HOME=\"${ORACLE_HOME}\"
ORACLE_SID=\"XE\"
export ORACLE_HOME
export ORACLE_SID
export PATH=\$PATH:${ORACLE_HOME}/bin">> /home/oracle/.bash_profile

fi

sudo -iu oracle MASTERPWD=${MASTERPWD} bash /vagrant/provision/sql/oracle_sakila_inst.sh

# Create PostgreSQL FDW
# Needed because of llvm version postgresql is compiled with
yum install -y  centos-release-scl git
yum install -y llvm-toolset-7.x86_64 postgresql${PGVERSION}-devel gcc
# Instant client
yum install -y /vagrant/oracle/*instantclient*rpm
export PATH=${PATH}:/usr/pgsql-${PGVERSION}/bin/
mkdir /oci
# For compilation
ln -sf /usr/include/oracle/18.5/client64/ /oci/include
ln -sf /usr/lib/oracle/18.5/client64/lib/libclntsh.so /usr/lib/libclntsh.so
# For postgresql Runtime
echo "/usr/lib/oracle/18.5/client64/lib/" > /etc/ld.so.conf.d/oracle_client_libs.conf
ldconfig /usr/lib/oracle/18.5/client64/lib/

# Download fdw + compile it
mkdir -p ${BUILD}
cd ${BUILD}
if [ ${PGVERSION} -eq "12" ]; then
git clone https://github.com/laurenz/oracle_fdw.git
cd oracle_fdw
else
curl -LsO https://github.com/laurenz/oracle_fdw/archive/ORACLE_FDW_2_1_0.tar.gz
tar xf ORACLE_FDW_2_1_0.tar.gz
cd oracle_fdw-ORACLE_FDW_2_1_0
fi
make
make install

sudo -iu postgres MASTERPWD=${MASTERPWD} bash /vagrant/provision/oracle_sakila_fdw.sh

# Ora_migrator
mkdir -p ${BUILD}
cd ${BUILD}
yum install -y git
git clone https://github.com/cybertec-postgresql/ora_migrator.git
cd ora_migrator
make install

# orafce
mkdir -p ${BUILD}
cd ${BUILD}
curl -LsO https://github.com/orafce/orafce/archive/VERSION_3_8_0.tar.gz
tar xf VERSION_3_8_0.tar.gz 
cd orafce-VERSION_3_8_0/
make install

sudo -iu postgres MASTERPWD=${MASTERPWD} bash /vagrant/provision/oracle_sakila_ora_migrator.sh

#ora2pg
mkdir -p ${BUILD}
cd ${BUILD}
# Download DBD::Oracle
yum install -y perl-ExtUtils-MakeMaker.noarch perl-DBI rh-perl524-perl perl-DBD-Pg.x86_64
curl -LsO https://cpan.metacpan.org/authors/id/M/MJ/MJEVANS/DBD-Oracle-1.80.tar.gz
tar xf  DBD-Oracle-1.80.tar.gz
cd DBD-Oracle-1.80
export LD_LIBRARY_PATH=${ORACLE_HOME}/lib
perl Makefile.PL
make 
make install
# we do this because yum will claim for DBD::Oracle 
# whereas it's already installed !
yumdownloader ora2pg
rpm -Uhv --nodeps  ora2pg-20.0-1.rhel7.noarch.rpm 

for CBASE in "sakila_ora_ora2pg" "sakila_ora_ora2pg_better"; do
  sudo -iu postgres MASTERPWD=${MASTERPWD} CBASE=${CBASE} bash /vagrant/provision/oracle_sakila_ora2pg.sh
  sudo -iu attendee MASTERPWD=${MASTERPWD} CBASE=${CBASE} bash /vagrant/provision/oracle_sakila_ora2pg_stage2.sh
done