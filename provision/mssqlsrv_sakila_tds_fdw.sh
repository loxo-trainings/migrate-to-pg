# Create attendee if needed, normally done in postgresql.sh
# createuser attendee;
CBASE="sakila_mssql_tds_fdw"
createdb -O attendee ${CBASE}
sed -e 's/@@MASTERPWD@@/'${MASTERPWD}'/g' /vagrant/provision/sql/mssqlsrv_sakila_tds_fdw.sql |tee /vagrant/exercises/mssql/fdw/00_create_fdw.sql| psql ${CBASE}