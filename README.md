# migrate-to-pg

Resource sfor PGConf.eu 2019 training.

This builds a Vagrant virtual machine using Virtual Box.

This VM will contain, after creation  :

  * PostgreSQL 11
  * MariaDB
  * Microsoft SQL Server 2019
  * Oracle Database 18c
  * oracle_fdw, tds_fdw, mysql_fdw
  * pgloader
  * ora_migrator
  * ora2pg
  * pg_chameleon
  * Exercises to migrate JOOQ sakila stored into each douce db.

# Disclaimer

Do not use in production.

# Manual operations before provisioning

## Download IBM DB2 Express C edition

Link : [here](https://www-01.ibm.com/marketing/iwm/iwm/web/download.do?source=swg-db2expressc&mhsrc=ibmsearch_a&mhq=DB2%20express%20C&S_TACT=000000TA&pageType=urx&S_PKG=ov48105&lang=en_US&dlmethod=http)

Put the file in `db2` directory and rename it `db2expressc.tar.gz`.

## Download Oracle XE

Link : [here](https://www.oracle.com/technetwork/database/database-technologies/express-edition/downloads/index.html)

Choose : Oracle Database 18c Express Edition for Linux x64
And also : Oracle Database Preinstall RPM for RHEL and CentOS  **Release 7**

Put the file in `oracle` directory.

You also need Instant client from Oracle (18.5) and you'll need to download instant client rpm from ORACLE network [here](https://www.oracle.com/database/technologies/instant-client/linux-x86-64-downloads.html)

Choose "oracle-instantclient18.5-basic-18.5.0.0.0-3.x86_64.rpm" and  "oracle-instantclient18.5-devel-18.5.0.0.0-3.x86_64.rpm"

Put both files in `oracle` directory.

## SSHFS

As assets are large, we recommand you install vagrant sshfs plugin. For this, you need to install it that way :

`vagrant plugin install vagrant-sshfs`

If you're not able (or don't want) to install vagrant-ssh, change Vagrantfile and comment sshfs engine for synced folders and uncomment rsync, that way :

```yaml
  config.vm.synced_folder ".", "/vagrant"
#  config.vm.synced_folder ".", "/vagrant", type:"sshfs"
```

# Provision

`vagrant up` if launched for the first time will create and provision the vm.

If already created (or something went wrong in up) you'll need to `vagrant provision` an already up VM.

Provisionning is split into two parts :

  * Engine installation and setup driven by identified shell scripts in `provision` directory
  * Database creation and schema initialization SQL script in `provision/sql` directory (engine name are clearly visible)

# Vagrnat up finished

Once finished (and successfull), the virtual machine could be run both through vagrant or VirtualBox.

## VM users

`attendee` user with `migrationTraining19` password is to be used with :

  * attendee is sudoer
  * database could be connected from outside

`oracle`, `mssql` users are vendor specific users, automatically created by install software packages.


`attendee`can run all the exercises scripts contained into the `~attendee/exercises` directory.

`ora2pg` skeleton are located in attendee `~attendee/sakila_ora_ora2pg*`

