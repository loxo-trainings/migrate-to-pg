#!/bin/bash

# Retrieve tools from internet
# Windows, Mac, Linux
wget -q https://dbeaver.io/files/dbeaver-ce-latest-x86_64-setup.exe
wget -q https://dbeaver.io/files/dbeaver-ce-latest-installer.pkg
wget -q https://dbeaver.io/files/dbeaver-ce-latest-linux.gtk.x86_64.tar.gz

# Drivers JDBC
#SqlServer
mkdir sqlsrv
cd sqlsrv
wget -q https://download.microsoft.com/download/0/2/A/02AAE597-3865-456C-AE7F-613F99F850A8/sqljdbc_6.0.8112.200_enu.tar.gz
wget -q https://download.microsoft.com/download/0/2/A/02AAE597-3865-456C-AE7F-613F99F850A8/sqljdbc_6.0.8112.200_enu.exe
cd ..
# Mariadb
mkdir mariadb
cd mariadb
# MariaDB
wget -q -O mariadb-java-client-2.4.2-sources.jar "https://downloads.mariadb.org/interstitial/connector-java-2.4.2/mariadb-java-client-2.4.2-sources.jar/from/http%3A//ftp.igh.cnrs.fr/pub/mariadb/"
cd ..
